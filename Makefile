PRJ_NAME = diy-vt100
PRJ_DIR = .
SRC_DIR = $(PRJ_DIR)/src
BUILD_DIR = $(PRJ_DIR)/build
LIB_DIR = $(PRJ_DIR)/libs
EXTRA_DIR = $(PRJ_DIR)/extra

# Linker script
LDSCRIPT = $(EXTRA_DIR)/stm32f767.ld

CFLAGS += -mtune=cortex-m7 -mcpu=cortex-m7 -mfloat-abi=hard -mfpu=fpv5-sp-d16
CFLAGS += -mlittle-endian -mthumb
CFLAGS += -I$(LIB_DIR)/unicore-mx/include -DSTM32F7
CFLAGS += -I$(SRC_DIR)
CFLAGS += -Wall -Werror -Wno-main
CFLAGS += -DDEBUG_BUILD
CFLAGS += -Ofast
CFLAGS += -g

LFLAGS += -nostartfiles -T $(LDSCRIPT) -nostdlib -Wl,--print-gc-sections
LFLAGS += -L$(LIB_DIR)/unicore-mx/lib

LIBS += -lucmx_stm32f7 -lc -lgcc -lnosys

OOCD = openocd

# Executables
ARCH = arm-none-eabi-
CC = $(ARCH)gcc
LD = $(ARCH)ld -v
AS = $(ARCH)as
OBJCPY = $(ARCH)objcopy
OBJDMP = $(ARCH)objdump
GDB = $(ARCH)gdb

CPFLAGS = --output-target=binary
ODFLAGS	= -x --syms

OBJS  = setup.o uart.o state.o screen.o bell.o
OBJS += hw-led.o hw-main.o hw-misc.o hw-screen.o hw-setting.o hw-usb.o
OBJS += hw-uart.o hw-ps2.o hw-bell.o hw-led.o hw-debug.o hw-font.o
OBJS += hw-eeprom.o hw-delay.o hw-sound.o hw-button.o hw-splash.o
OBJS += hw-touch.o
OBJS += generic-ps2-kbd.o generic-usb-kbd.o generic-cqueue.o generic-bit-array.o
OBJS += vt100-buffer.o vt100-report.o vt100-cursor.o
OBJS += vt100-margin.o vt100-tab.o vt100-attribute.o vt100-misc.o
OBJS += vt52-misc.o

MKDIR = mkdir

# Targets
all: bin

# create build directory
init:
	@printf "Creating build directory\n"
	@-$(MKDIR) $(BUILD_DIR)
	@printf "Building unicore-mx"
	@cd $(LIB_DIR)/unicore-mx; $(MAKE) TARGETS="stm32/f7"

clean:
	rm -rf $(BUILD_DIR)/*

ihex: elf
	$(OBJCPY) --output-target=ihex 				\
		$(BUILD_DIR)/$(PRJ_NAME).elf			\
		$(BUILD_DIR)/$(PRJ_NAME).ihex

bin: elf
	@printf "BIN $(PRJ_NAME).bin\n"
	@$(OBJCPY) $(CPFLAGS) $(BUILD_DIR)/$(PRJ_NAME).elf $(BUILD_DIR)/$(PRJ_NAME).bin

%.o: $(SRC_DIR)/%.c
	@printf "CC $<\n"
	@$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ -c $<

hw-%.o: $(SRC_DIR)/hw/%.c
	@printf "CC $<\n"
	@$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ -c $<

vt100-%.o: $(SRC_DIR)/vt100/%.c
	@printf "CC $<\n"
	@$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ -c $<

vt52-%.o: $(SRC_DIR)/vt52/%.c
	@printf "CC $<\n"
	@$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ -c $<

generic-%.o: $(SRC_DIR)/generic/%.c
	@printf "CC $<\n"
	@$(CC) $(CFLAGS) -o $(BUILD_DIR)/$@ -c $<

elf: $(LDSCRIPT) $(OBJS)
	@printf "ELF $(PRJ_NAME).elf \n"
	@$(CC) $(CFLAGS) $(LFLAGS) $(BUILD_DIR)/*.o  \
		--static $(LIBS) -o $(BUILD_DIR)/$(PRJ_NAME).elf

install: burn
flash: burn

burn:
	@printf "Installing on device (Openocd, STLINK v2)"
	@$(OOCD) -f interface/stlink-v2.cfg							\
		-c "transport select hla_swd"							\
		-f "target/stm32f7x.cfg"								\
		-c "init"												\
		-c "reset halt"											\
		-c "stm32f2x mass_erase 0"								\
		-c "flash write_image $(BUILD_DIR)/$(PRJ_NAME).elf"		\
		-c "reset"												\
		-c "shutdown"
