#!/bin/python

import png

"""
Note: do ensure that all the images are 1bit per pixel.
use the make-png-1bpp.sh script to do so.
"""

def file_to_hex(name):
	r = png.Reader(name)
	img = r.read()

	w = img[0]
	h = img[1]
	data = list(img[2])

	# convert the 2d array into 1D array
	res = []
	for i in range(h):
		for d in data[i]:
			res.append(d)

	# convert the 1d array to 8bit hex array
	outp = []
	while res:
		v = ("%i"*8) % tuple(res[0:8][::-1]) # convert data to a binary string
		v = int(v, 2) # convert to int
		outp.append(hex(v)) # int as hex string
		res = res[8:] # remove the used

	return ', '.join(outp)


print("uint8_t font_data[96][15] = {")

for i in range(32, 128):
	print("{%s}, /* %i */" % (file_to_hex("char%i.png" % i), i))

print("};")
