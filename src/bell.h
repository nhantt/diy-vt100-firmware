/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_BELL_H
#define DIY_VT100_BELL_H

/**
 * When a key is pressed
 * This function should return immediately (non-blocking)
 */
void bell_keyclick(void);

/**
 * CTRL-G character received
 * This function should return immediately (non-blocking)
 */
void bell_code_recv(void);

/**
 * When margin has been approched
 * This function should return immediately (non-blocking)
 * To be called when cursor column has been changed.
 */
void bell_margin(void);

enum vt100_bell_type {
	VT100_BELL_SHORT,
	VT100_BELL_LONG
};

/**
 * Perform a VT100 short bell.
 * This function should return immediately (non-blocking)
 * @param type Bell type
 */
void hw_bell(enum vt100_bell_type bell_type);

#endif
