/*
 * This file is part of diy-VT100.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_CONFIG_H
#define DIY_VT100_CONFIG_H

#include "hw/hw.h"

//typedef uint<size>_t vt100_row;
//typedef uint<size>_t vt100_col;

/* [LCD] */
//#define SCREEN_ROW <CHAR>
//#define SCREEN_COL <CHAR>;
//#define SCREEN_WIDTH <PIXEL>
//#define SCREEN_HEIGHT <PIXEL>
//#define SCREEN_CHAR_HEIGHT <PIXEL>
//#define SCREEN_CHAR_WIDTH <PIXEL>

#if !defined(SCREEN_ROW)
# error "Please define SCREEN_ROW <value>"
#endif

#if !defined(SCREEN_COL)
# error "Please #define SCREEN_COL <value>"
#endif

#if !defined(SCREEN_COL)
# error "Please #define SCREEN_WIDTH <value>"
#endif

#if !defined(SCREEN_COL)
# error "Please #define SCREEN_HEIGHT <value>"
#endif

#if !defined(SCREEN_COL)
# error "Please #define SCREEN_CHAR_HEIGHT <value>"
#endif

#if !defined(SCREEN_COL)
# error "Please #define SCREEN_CHAR_WIDTH <value>"
#endif

/* [hardware] */
//#define SCREEN_BRIGHTNESS_MAX (maximum brightness)
//#define SCREEN_BRIGHTNESS_MIN (minimum brightness)

#if !defined(SCREEN_BRIGHTNESS_MAX)
# error "Please #define SCREEN_BRIGHTNESS_MIN <value>"
#endif

#if !defined(SCREEN_BRIGHTNESS_MAX)
# error "Please #define SCREEN_BRIGHTNESS_MIN <value>"
#endif

/* TODO: Currently the code implement AVO.
 * we can reduce memory consumption by half by not implementing AVO.
 * Not implementing do not require storing
 *  character set and multiple data attribute for each data.
 *
 * A single attribute is require to be store in non AVO mode.
 * This bit can be store in the MSB of the data since we are using 7bit
 * of the data array
 */

#if !defined(AVO_INSTALLED)
/* Disable AVO by default */
# define AVO_INSTALLED 0
#endif

#if (AVO_INSTALLED == 0)
# error "FIXME: (AVO_INSTALLED == 0) not fully implemented YET!"
#endif

#if !defined(DEBUG_BUILD)
/* Disable build by default */
# define DEBUG_BUILD 0
#endif

#if !defined(PARAM_QUEUE_SIZE)
# define PARAM_QUEUE_SIZE 32
#endif

#endif
