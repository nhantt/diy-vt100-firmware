#ifndef BIT_ARRAY_H
#define BIT_ARRAY_H

#include <stdint.h>
#include <stdbool.h>

void bitarr_high(uint8_t *data, unsigned index);
void bitarr_low(uint8_t *data, unsigned index);
void bitarr_write(uint8_t *data, unsigned index, bool value);
bool bitarr_read(const uint8_t *data, unsigned index);
void bitarr_flip(uint8_t *data, unsigned index);

#endif
