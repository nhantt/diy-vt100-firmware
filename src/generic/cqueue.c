/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "cqueue.h"
#include "debug.h"

/**
 * Pop data from circular @a queue
 * @param queue Queue
 * @return data
 * @warning assuming has data (no underflow)
 */
uint8_t cqueue_pop(volatile struct cqueue *queue)
{
	/* NOTE: using AND instead of %(modulo) */
	uint32_t head = queue->head;
	queue->head = (head + 1) & queue->mod_mask;
	queue->used--;

	return queue->data[head];
}

/**
 * Push @a data to circular @a queue
 * @param queue Queue
 * @param data Data
 * @warning assuming space is available (no overflow)
 */
void cqueue_push(volatile struct cqueue *queue, uint8_t data)
{
	/* NOTE: using AND instead of %(modulo) */
	uint32_t end = (queue->head + queue->used) & queue->mod_mask;
	queue->data[end] = data;
	queue->used++;
}
