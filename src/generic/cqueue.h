/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef CQUEUE_H
#define CQUEUE_H

#include "common.h"

struct cqueue {
	uint32_t head, used, mod_mask /* = size - 1 */;
	uint8_t *data;
};

#define CQUEUE_DATA_LEN(bits) (1 << (bits))
#define CQUEUE_MOD_MASK(bits) (CQUEUE_DATA_LEN(bits) - 1)

#define CQUEUE_HAS_DATA(ptr) ((ptr)->used)
#define CQUEUE_HALF_FULL(ptr) ((ptr)->used > ((ptr)->mod_mask / 2))
#define CQUEUE_EMPTY(ptr) (!CQUEUE_HAS_DATA(ptr))
#define CQUEUE_FULL(ptr) ((ptr)->used > (ptr)->mod_mask)

void cqueue_push(volatile struct cqueue *, uint8_t);
uint8_t cqueue_pop(volatile struct cqueue *);

#endif
