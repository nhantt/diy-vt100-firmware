/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "usb-kbd.h"
#include "vt100.h"
#include "bell.h"
#include "uart.h"
#include "setup.h"
#include "misc.h"
#include "screen.h"

#define KBD_MODIFIER HW_PRIV1

static void usbkbd_other_setup(uint8_t ch)
{
	switch (ch) {
	case '0':
		vt100_reset();
	break;
	case '2':
		setup_TAB_flip();
	break;
	case '3':
		setup_TABS_clear();
	break;
	case '4':
		setup_LOCAL();
	break;
	case '5':
		setup_switch();
	break;
	case '6':
		setup_value_flip();
	break;
	case '7':
		setup_uart_tx();
	break;
	case '8':
		setup_uart_rx();
	break;
	case '9':
		setup_DECCOLM();
	break;
	case ' ':
		setup_cursor_next();
	break;
	case 's':
		if (vt100.KBD_SHIFT) {
			setup_save();
		} else {
			return;
		}
	break;
	case 'r':
		if (vt100.KBD_SHIFT) {
			setup_recall();
		} else {
			return;
		}
	break;
	default:
	return;
	}

	bell_keyclick();
}

static void usbkbd_f1(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF1);
	}

	bell_keyclick();
}

static void usbkbd_f2(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF2);
	}

	bell_keyclick();
}

static void usbkbd_f3(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF3);
	}

	bell_keyclick();
}

static void usbkbd_f4(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_pfn(KEYPAD_PF4);
	}

	bell_keyclick();
}

/* working as SETUP key */
static void usbkbd_f5(void)
{
	setup();
	bell_keyclick();
}

static void usbkbd_caps(void)
{
	vt100.KBD_CAPS ^= true;
	bell_keyclick();
}

static void __usbkbd_mux_keypad_num_arrow(uint8_t num, uint8_t arrow)
{
	if (vt100.KBD_MODIFIER) {
		/* arrow key pressed */
		uart_send_arrow(arrow);
	} else {
		/* send accoring to Keypad mode */
		uart_send_keypad_num(num);
	}
}

static void usbkbd_keypad_0(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(0);
	}

	bell_keyclick();
}

static void usbkbd_keypad_1(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(1);
	}

	bell_keyclick();
}

static void usbkbd_keypad_2(void)
{
	if (vt100.SETUP_SHOW) {
		screen_brightness_dec();
	} else {
		__usbkbd_mux_keypad_num_arrow(2, ARROW_DOWN);
	}

	bell_keyclick();
}

static void usbkbd_keypad_3(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(3);
	}
}

static void usbkbd_keypad_4(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_prev();
	} else {
		__usbkbd_mux_keypad_num_arrow(4, ARROW_LEFT);
	}

	bell_keyclick();
}

static void usbkbd_keypad_5(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(5);
	}

	bell_keyclick();
}

static void usbkbd_keypad_6(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_next();
	} else {
		__usbkbd_mux_keypad_num_arrow(6, ARROW_RIGHT);
	}

	bell_keyclick();
}

static void usbkbd_keypad_7(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(7);
	}

	bell_keyclick();
}

static void usbkbd_keypad_8(void)
{
	if (vt100.SETUP_SHOW) {
		screen_brightness_inc();
	} else {
		__usbkbd_mux_keypad_num_arrow(8, ARROW_UP);
	}

	bell_keyclick();
}

static void usbkbd_keypad_9(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_num(9);
	}

	bell_keyclick();
}

static void usbkbd_keypad_dash(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send_keypad_dash();
	}

	bell_keyclick();
}

static void usbkbd_keypad_dot(void)
{
	if (vt100.KBD_MODIFIER) {
		uart_send(ASCII_DEL);
	} else {
		uart_send_keypad_dot();
	}

	bell_keyclick();
}

static void usbkbd_keypad_enter(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_next();
	} else {
		uart_send_keypad_enter();
	}

	bell_keyclick();
}

static void usbkbd_enter(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_first();
	} else {
		uart_send_return();
	}

	bell_keyclick();
}

static void usbkbd_backspace(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send(ASCII_BS);
	}

	bell_keyclick();
}

static void usbkbd_escape(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send(ASCII_ESCAPE);
	}

	bell_keyclick();
}

static void usbkbd_tab(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_tab();
	} else {
		uart_send(ASCII_TAB);
	}

	bell_keyclick();
}

static void usbkbd_scroll(void)
{
	if (vt100.SETUP_SHOW) {
		return;
	}

	if (vt100.XOFF_SEND) {
		uart_send(ASCII_XON);
		vt100.XOFF_SEND = false;
	} else {
		uart_send(ASCII_XOFF);
		vt100.XOFF_SEND = true;
	}

	vt100.XOFF_SCROLL = true;
}

static void usbkbd_arrow_up(void)
{
	if (vt100.SETUP_SHOW) {
		screen_brightness_inc();
	} else {
		uart_send_arrow(ARROW_UP);
	}

	bell_keyclick();
}

static void usbkbd_arrow_down(void)
{
	if (vt100.SETUP_SHOW) {
		screen_brightness_dec();
	} else {
		uart_send_arrow(ARROW_DOWN);
	}

	bell_keyclick();
}

static void usbkbd_arrow_left(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_prev();
	} else {
		uart_send_arrow(ARROW_LEFT);
	}

	bell_keyclick();
}

static void usbkbd_arrow_right(void)
{
	if (vt100.SETUP_SHOW) {
		setup_cursor_next();
	} else {
		uart_send_arrow(ARROW_RIGHT);
	}

	bell_keyclick();
}

static void usbkbd_break(void)
{
	if (vt100.KBD_CTRL) {
		uart_send_answerback_string();
	}

	bell_keyclick();
}

static void usbkbd_cp_delete(void)
{
	if (!vt100.SETUP_SHOW) {
		uart_send(ASCII_DEL);
	}

	bell_keyclick();
}

typedef void (*_scan_code_callback)(void);

struct _scan_code {
	/*
	 * 0 = do not exists
	 * 1 = character is affected by caps status
	 * other = call
	 */
	_scan_code_callback callback;

	/* 0:main, 1:replacement */
	uint8_t ch[2];
};

#define SCAN_CODE_SIZE 100

#define SCI_TYPE_NUMBER (NULL + 2)
#define SCI_TYPE_SPECIAL (NULL + 2)
#define SCI_TYPE_ALPHABET (NULL + 1)
#define SCI_TYPE_IGNORE NULL

#define SCI_NUMBER(ch, ch_alt)		{SCI_TYPE_NUMBER, {ch, ch_alt}}
#define SCI_SPECIAL(ch, ch_alt)		{SCI_TYPE_NUMBER, {ch, ch_alt}}
#define SCI_ALPHABET(ch, ch_alt)	{SCI_TYPE_NUMBER, {ch, ch_alt}}
#define SCI_CALLBACK(cb)			{cb}
#define SCI_IGNORE					{SCI_TYPE_IGNORE}

#define IS_VALID_CALLBACK(fn) ((fn) > (_scan_code_callback)(NULL + 2))

const static struct _scan_code scan_code[SCAN_CODE_SIZE] = {
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_IGNORE,
	SCI_ALPHABET('a', 'A'),
	SCI_ALPHABET('b', 'B'),
	SCI_ALPHABET('c', 'C'),
	SCI_ALPHABET('d', 'D'),
	SCI_ALPHABET('e', 'E'),
	SCI_ALPHABET('f', 'F'),
	SCI_ALPHABET('g', 'G'),
	SCI_ALPHABET('h', 'H'),
	SCI_ALPHABET('i', 'I'),
	SCI_ALPHABET('j', 'J'),
	SCI_ALPHABET('k', 'K'),
	SCI_ALPHABET('l', 'L'),
	SCI_ALPHABET('m', 'M'),
	SCI_ALPHABET('n', 'N'),
	SCI_ALPHABET('o', 'O'),
	SCI_ALPHABET('p', 'P'),
	SCI_ALPHABET('q', 'Q'),
	SCI_ALPHABET('r', 'R'),
	SCI_ALPHABET('s', 'S'),
	SCI_ALPHABET('t', 'T'),
	SCI_ALPHABET('u', 'U'),
	SCI_ALPHABET('v', 'V'),
	SCI_ALPHABET('w', 'W'),
	SCI_ALPHABET('x', 'X'),
	SCI_ALPHABET('y', 'Y'),
	SCI_ALPHABET('z', 'Z'),
	SCI_NUMBER('1', '!'),
	SCI_NUMBER('2', '@'),
	SCI_NUMBER('3', '#'),
	SCI_NUMBER('4', '$'),
	SCI_NUMBER('5', '%'),
	SCI_NUMBER('6', '^'),
	SCI_NUMBER('7', '&'),
	SCI_NUMBER('8', '*'),
	SCI_NUMBER('9', '('),
	SCI_NUMBER('0', ')'),
	SCI_CALLBACK(usbkbd_enter),
	SCI_CALLBACK(usbkbd_escape),//SCI_SPECIAL(ASCII_ESCAPE, ASCII_ESCAPE),
	SCI_CALLBACK(usbkbd_backspace),//SCI_SPECIAL(ASCII_BS, ASCII_BS),
	SCI_CALLBACK(usbkbd_tab),//SCI_SPECIAL(ASCII_TAB, ASCII_TAB),
	SCI_ALPHABET(' ', ' '),
	SCI_SPECIAL('-', '_'),
	SCI_SPECIAL('=', '+'),
	SCI_SPECIAL('[', '{'),
	SCI_SPECIAL(']', '}'),
	SCI_SPECIAL('\\', '|'),
	SCI_IGNORE, /* (INT 2) */
	SCI_SPECIAL(';', ':'),
	SCI_SPECIAL('\'', '"'),
	SCI_SPECIAL('`', '~'),
	SCI_SPECIAL(',', '<'),
	SCI_SPECIAL('.', '>'),
	SCI_SPECIAL('/', '?'),
	SCI_CALLBACK(usbkbd_caps),
	SCI_CALLBACK(usbkbd_f1),
	SCI_CALLBACK(usbkbd_f2),
	SCI_CALLBACK(usbkbd_f3),
	SCI_CALLBACK(usbkbd_f4),
	SCI_CALLBACK(usbkbd_f5),
	SCI_IGNORE, /* F6 */
	SCI_IGNORE, /* F7 */
	SCI_IGNORE, /* F8 */
	SCI_IGNORE, /* F9 */
	SCI_IGNORE, /* F10 */
	SCI_IGNORE, /* F11 */
	SCI_IGNORE, /* F12 */
	SCI_IGNORE, /* PrtScr */
	SCI_CALLBACK(usbkbd_scroll),
	SCI_CALLBACK(usbkbd_break),
	SCI_IGNORE, /* Ins CP */
	SCI_IGNORE, /* Home CP */
	SCI_IGNORE, /* PgUp CP */
	SCI_CALLBACK(usbkbd_cp_delete), /* Del CP */
	SCI_IGNORE, /* End CP */
	SCI_IGNORE, /* PgDn CP */
	SCI_CALLBACK(usbkbd_arrow_right),
	SCI_CALLBACK(usbkbd_arrow_left),
	SCI_CALLBACK(usbkbd_arrow_down),
	SCI_CALLBACK(usbkbd_arrow_up),
	SCI_IGNORE, /* num lock */
	SCI_SPECIAL('/', '/'), /* Original VT100 keyboard didn't had this */
	SCI_SPECIAL('*', '*'),
	SCI_CALLBACK(usbkbd_keypad_dash),
	SCI_SPECIAL('+', '+'),
	SCI_CALLBACK(usbkbd_keypad_enter),
	SCI_CALLBACK(usbkbd_keypad_1),
	SCI_CALLBACK(usbkbd_keypad_2),
	SCI_CALLBACK(usbkbd_keypad_3),
	SCI_CALLBACK(usbkbd_keypad_4),
	SCI_CALLBACK(usbkbd_keypad_5),
	SCI_CALLBACK(usbkbd_keypad_6),
	SCI_CALLBACK(usbkbd_keypad_7),
	SCI_CALLBACK(usbkbd_keypad_8),
	SCI_CALLBACK(usbkbd_keypad_9),
	SCI_CALLBACK(usbkbd_keypad_0),
	SCI_CALLBACK(usbkbd_keypad_dot),
};

void usbkbd_decode(const uint8_t *data, unsigned len)
{
	uint8_t modif = data[0];

	/* data at index 1 is vendor specific, ignoring */

	/* left or right "shift" */
	vt100.KBD_SHIFT = !!(modif & 0x22);

	/* left or right "ctrl" */
	vt100.KBD_CTRL = !!(modif & 0x11);

	for (unsigned i = 2; i < len; i++) {
		uint8_t raw = data[i];
		if (raw >= SCAN_CODE_SIZE) {
			continue;
		}

		const struct _scan_code *item = &scan_code[raw];
		if (IS_VALID_CALLBACK(item->callback)) {
			item->callback();
			continue;
		}

		if (item->callback == SCI_TYPE_IGNORE) {
			continue;
		}

		if (vt100.SETUP_SHOW) {
			usbkbd_other_setup(item->ch[0]);
			continue;
		}

		if (vt100.KBD_CTRL) {
			uint8_t ch = item->ch[1];
			switch (ch) {
			case '{':
				ch = '[';
			break;
			case '|':
				ch = '\\';
			break;
			case '}':
				ch = ']';
			break;
			}

			if (ch >= '@' && ch <= '_') {
				uart_send(ch - '@');
				bell_keyclick();
			}

			continue;
		}

		uint8_t ch_alt = 0;

		/* caps affected */
		if (item->callback == SCI_TYPE_ALPHABET && vt100.KBD_CAPS) {
			ch_alt |= 1;
		}

		if (vt100.KBD_SHIFT) {
			ch_alt ^= 1;
		}

		uart_send(item->ch[ch_alt]);
		bell_keyclick();
	}
}
