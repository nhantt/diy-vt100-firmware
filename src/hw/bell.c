/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/dma.h>
#include <unicore-mx/stm32/dac.h>
#include "bell.h"
#include "hw.h"
#include "screen.h"
#include "vt100.h"

#define SAMPLE_COUNT 8000

extern const uint8_t bell_long_waveform[SAMPLE_COUNT];

/**
 * Setup the speaker
 */
void bell_setup(void)
{
	/* Set PA5 for DAC channel 2 to analogue, ignoring drive mode. */
	gpio_mode_setup(GPIOA, GPIO_MODE_ANALOG, GPIO_PUPD_NONE, GPIO5);

	/* Setup DMA Stream 6 to DAC2 (Channel 7) */
	DMA1_S6NDTR = SAMPLE_COUNT;
	DMA1_S6M0AR = (void *) bell_long_waveform;
	DMA1_S6PAR = &DAC_DHR8R2;
	DMA1_S6CR = DMA_SxCR_CHSEL_7 | DMA_SxCR_CIRC | DMA_SxCR_MINC |
				DMA_SxCR_PSIZE_8BIT | DMA_SxCR_MSIZE_8BIT |
				DMA_SxCR_DIR_MEM_TO_PERIPHERAL | DMA_SxCR_PL_LOW |
				DMA_SxCR_EN;

	/* Amplifier EN pin GPIO configuration */
	/* This is above my head.
	 * Moving this block of code somewhere else
	 * cause problem with DMA/DAC { */
	gpio_clear(GPIOB, GPIO13);
	gpio_set_output_options(GPIOB, GPIO_OTYPE_OD, GPIO_OSPEED_2MHZ, GPIO13);
	gpio_mode_setup(GPIOB, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO13);
	/* } */

	/* Setup DAC */
	DAC_CR = DAC_CR_EN2 | DAC_CR_TEN2 | DAC_CR_TSEL2_T6 | DAC_CR_DMAEN2;
	DAC_DHR8R2 = 0x7F;

	/* Setup Timer 6 to trigger DAC  */
	TIM6_ARR = 1000 - 1; /* 48 KHz @ 48MHz (APB1) */
	TIM6_CR2 = TIM_CR2_MMS_UPDATE;

	/* Check bell output (After 83.3bar/2 ms) */
	TIM7_PSC = 1000 - 1; /* clock divider, 48KHz @ 48MHz (ABP1) */
	TIM7_ARR = SAMPLE_COUNT / 2; /* Total samples */
	TIM7_DIER = TIM_DIER_UIE; /* enable update interrrupt */

	/* Enable {TIM7} IRQ */
	nvic_enable_irq(NVIC_TIM7_IRQ);
}

volatile unsigned bell_count = 0;

void hw_bell(enum vt100_bell_type bell_type)
{
	if (TIM6_CR1 & TIM_CR1_CEN) {
		/* Already running. */
		if (bell_type == VT100_BELL_LONG && bell_count < 2) {
			bell_count = 2;
		}
		return;
	}

	bell_count = (bell_type == VT100_BELL_LONG) ? 2 : 1;
	DMA1_S6CR |= DMA_SxCR_EN;
	TIM6_CR1 = TIM_CR1_CEN;
	TIM7_CR1 = TIM_CR1_CEN;
}

void tim7_isr(void)
{
	TIM7_SR = 0;

	if (bell_count > 1) {
		bell_count--;
		return;
	}

	/* Disable bell */
	DMA1_S6CR &= ~DMA_SxCR_EN;
	TIM6_CR1 = 0;
	TIM7_CR1 = 0;
	DAC_DHR8R2 = 0x7F;
	bell_count = 0;
}
