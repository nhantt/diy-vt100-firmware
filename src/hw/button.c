/*
 * This file is part of diy-VT100.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/exti.h>
#include <unicore-mx/stm32/timer.h>
#include "setup.h"
#include "bell.h"
#include "debug.h"

/*
 * Software debouncing is implemented using timer, exti and gpio.
 * When a interrupt comes in, timer is started in one pulse mode.
 * When the timer timeout, it start setup mode
 */

void button_setup(void)
{
	/* Timer configuration (Consume 250ms after starting) */
	TIM11_PSC = 12000 - 1; /* 4KHz (250us) @ 48MHz (APB2)  */
	TIM11_ARR = 1000;

	/* Pin configuration */
	gpio_mode_setup(GPIOD, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO9);

	/* EXTI configuration */
	exti_select_source(EXTI9, GPIOD);
	exti_set_trigger(EXTI9, EXTI_TRIGGER_FALLING);
	exti_reset_request(EXTI9);
	exti_enable_request(EXTI9);

	/* NVIC configuration */
	nvic_enable_irq(NVIC_EXTI9_5_IRQ);
}

void button_poll(void)
{
	if (TIM11_SR & TIM_SR_UIF) {
		TIM11_SR = 0;
		setup();
		bell_keyclick();
	}
}

void exti9_5_isr(void)
{
	if (!(GPIOD_IDR & GPIO9)) {
		TIM11_CR1 = TIM_CR1_CEN | TIM_CR1_OPM;
	}

	EXTI_PR = EXTI9;
}
