/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/usart.h>
#include <unicore-mx/stm32/gpio.h>

#include "debug.h"

#if (DEBUG_BUILD == 1)

void debug_setup(void)
{
	/* USART TX */
	gpio_set_af(GPIOE, GPIO_AF8, GPIO1);
	gpio_set_output_options(GPIOE, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, GPIO1);
	gpio_mode_setup(GPIOE, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO1);

	usart_set_baudrate(UART8, 921600);
	usart_set_databits(UART8, 8);
	usart_set_flow_control(UART8, USART_FLOWCONTROL_NONE);
	usart_set_mode(UART8, USART_MODE_TX);
	usart_set_parity(UART8, USART_PARITY_NONE);
	usart_set_stopbits(UART8, USART_STOPBITS_1);

	usart_enable(UART8);
}

/**
 * Output string @a arg
 */
void debug_puts(const char *arg)
{
	while (*arg != '\0') {
		usart_wait_send_ready(UART8);
		usart_send(UART8, *arg++);
	}
}

void debug_vprintf(const char *fmt, va_list va)
{
	char db[128];
	if (vsnprintf(db, sizeof(db), fmt, va) > 0) {
		debug_puts(db);
	}
}

void debug_printf(const char *fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	debug_vprintf(fmt, va);
	va_end(va);
}

/* used by USB host library for printing */

void usbh_log_puts(const char *str)
{
	debug_puts(str);
}

void usbh_log_printf(const char *fmt, ...)
{
	va_list va;
	va_start(va, fmt);
	debug_vprintf(fmt, va);
	va_end(va);
}

#endif
