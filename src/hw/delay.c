/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "delay.h"
#include <unicore-mx/stm32/timer.h>

/**
 * Pass @a div = 48 for 1uS cycle
 * Pass @a div = (48*100) for 100uS (0.1ms) cycle
 * Pass @a div = (48*500) for 500uS (0.5ms) cycle
 * Pass @a div = (48*1000) for 1000uS (1ms) cycle
 */
void delay(uint16_t div, uint16_t count)
{
	/* FIXME: Sleep the core when in busy wait */
	TIM13_PSC = div - 1; /* @ 48MHz (APB1) */
	TIM13_ARR = count;
	TIM13_EGR = TIM_EGR_UG; /* data got update */
	TIM13_SR = 0;
	TIM13_CR1 = TIM_CR1_CEN | TIM_CR1_OPM;
	while (TIM13_CR1 & TIM_CR1_CEN);
}
