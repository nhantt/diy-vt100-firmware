/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/i2c.h>
#include <unicore-mx/stm32/memorymap.h>
#include <unicore-mx/stm32/gpio.h>
#include "eeprom.h"
#include "debug.h"

/* 7bit */
#define EEPROM_ADDR 0x50

void eeprom_setup(void)
{
	/* Write protect */
	gpio_set(GPIOD, GPIO11);
	gpio_set_output_options(GPIOD, GPIO_OTYPE_OD, GPIO_OSPEED_2MHZ, GPIO11);
	gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO11);

	/* I2C pins */
	gpio_set_af(GPIOD, GPIO_AF4, GPIO12 | GPIO13);
	gpio_set_output_options(GPIOD, GPIO_OTYPE_OD, GPIO_OSPEED_2MHZ, GPIO12 | GPIO13);
	gpio_mode_setup(GPIOD, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO12 | GPIO13);

	/* Standard mode (Input clock = 48MHz)
	 * I2CCLK = 48MHz / (11 + 1) = 4 MHz
	 * SCLL = 5.0 μs  ((19 + 1) / 4MHz)
	 * SCLH = 4.0 μs  ((15 + 1) / 4MHz)
	 * SDADEL = 500 ns (2 / 4MHz)
	 * SCLDEL = 1250 ns ((4 + 1) / 4MHz)
	 */
	I2C4_TIMINGR = (11 << I2C_TIMINGR_PRESC_SHIFT) |
					(19 << I2C_TIMINGR_SCLL_SHIFT) |
					(15 << I2C_TIMINGR_SCLH_SHIFT) |
					(2 << I2C_TIMINGR_SDADEL_SHIFT) |
					(4 << I2C_TIMINGR_SCLDEL_SHIFT);
	I2C4_CR1 = I2C_CR1_PE;

#if (DEBUG_BUILD == 1)
	/* Try to read 1 byte from slave to check if there any */
	I2C4_CR2 = I2C_CR2_AUTOEND | I2C_CR2_START | I2C_CR2_RD_WRN |
				(1 << I2C_CR2_NBYTES_SHIFT) | (EEPROM_ADDR << 1);

	while (!(I2C4_ISR & I2C_ISR_STOPF));
	bool detected = !(I2C4_ISR & I2C_ISR_NACKF);
	LOG_LN(detected ? "EEPROM detected!" : "EEPROM not detected");
#endif
}

void eeprom_write_protect(bool enable)
{
	if (enable) {
		gpio_set(GPIOD, GPIO11);
	} else {
		gpio_clear(GPIOD, GPIO11);
	}
}

uint8_t eeprom_write(uint8_t mem_addr, const uint8_t *data, uint8_t len)
{
	uint8_t written = 0;

#if (DEBUG_BUILD == 1)
	LOG("EEPROM WRITE: ");
	for (uint8_t i = 0; i < len; i++) {
		LOGF("0x%"PRIx8" ", data[i]);
	}
	LOG(NEW_LINE);
#endif

	/* Clear all flags */
	I2C4_ICR = I2C_ICR_ALERTCF | I2C_ICR_TIMOUTCF | I2C_ICR_PECCF |
				I2C_ICR_OVRCF | I2C_ICR_ARLOCF | I2C_ICR_BERRCF |
				I2C_ICR_STOPCF | I2C_ICR_NACKCF | I2C_ICR_ADDRCF;

	/* Send START (WARN: @a len <= 254) */
	I2C4_CR2 = I2C_CR2_START | ((1 + len) << I2C_CR2_NBYTES_SHIFT) |
				(EEPROM_ADDR << 1) | I2C_CR2_AUTOEND;

	for (;;) {
		if (I2C4_ISR & I2C_ISR_NACKF) {
			LOG_LN("eeprom_write(): got NACK in addressing stage");
			goto done;
		}

		if (I2C4_ISR & I2C_ISR_TXIS) {
			break;
		}
	}

	/* Write memory address */
	I2C4_TXDR = mem_addr;

	while (!(I2C4_ISR & I2C_ISR_TXE));
	if (I2C4_ISR & I2C_ISR_NACKF) {
		LOG_LN("eeprom_write(): NACK flag set after sending memory address");
		goto done;
	}

	while (written < len) {
		if (I2C4_ISR & I2C_ISR_TXIS) {
			I2C4_TXDR = data[written++];
		}

		if (I2C4_ISR & I2C_ISR_NACKF) {
			LOG_LN("eeprom_write(): NACK flag set after sending data");
			break;
		}
	}

	done:
	if (written < len) {
		LOGF_LN("eeprom_write(): only written %"PRIu8" out of %"PRIu8" bytes", written, len);
	}

	return written;
}

uint8_t eeprom_read(uint8_t mem_addr, uint8_t *data, uint8_t len)
{
	uint8_t readed = 0;

	/* Clear all flags */
	I2C4_ICR = I2C_ICR_ALERTCF | I2C_ICR_TIMOUTCF | I2C_ICR_PECCF |
				I2C_ICR_OVRCF | I2C_ICR_ARLOCF | I2C_ICR_BERRCF |
				I2C_ICR_STOPCF | I2C_ICR_NACKCF | I2C_ICR_ADDRCF;

	/* Flused already received data */
	while (I2C4_ISR & I2C_ISR_RXNE) {
		uint8_t tmp __attribute__((unused)) = I2C4_RXDR;
	}

	/* Send START */
	I2C4_CR2 = I2C_CR2_START | (1 << I2C_CR2_NBYTES_SHIFT) | (EEPROM_ADDR << 1);

	for (;;) {
		if (I2C4_ISR & I2C_ISR_NACKF) {
			LOG_LN("eeprom_read(): got NACK in addressing stage");
			goto done;
		}

		if (I2C4_ISR & I2C_ISR_TXIS) {
			break;
		}
	}

	/* Write memory address */
	I2C4_TXDR = mem_addr;

	while (!(I2C4_ISR & I2C_ISR_TC));
	if (I2C4_ISR & I2C_ISR_NACKF) {
		LOG_LN("eeprom_read(): NACK flag set after sending memory address");
		goto done;
	}

	/* Clear all flags */
	I2C4_ICR = I2C_ICR_ALERTCF | I2C_ICR_TIMOUTCF | I2C_ICR_PECCF |
				I2C_ICR_OVRCF | I2C_ICR_ARLOCF | I2C_ICR_BERRCF |
				I2C_ICR_STOPCF | I2C_ICR_NACKCF | I2C_ICR_ADDRCF;

	/* Send a RESTART */
	I2C4_CR2 = (EEPROM_ADDR << 1) | I2C_CR2_RD_WRN | I2C_CR2_START |
				(len << I2C_CR2_NBYTES_SHIFT) | I2C_CR2_AUTOEND;

	/* Read data */
	while (readed < len) {
		if (I2C4_ISR & I2C_ISR_NACKF) {
			LOG_LN("eeprom_read(): got NACK while reading data");
			break;
		}

		if (I2C4_ISR & I2C_ISR_RXNE) {
			data[readed++] = I2C4_RXDR;
		}
	}

#if (DEBUG_BUILD == 1)
	LOG("EEPROM READ: ");
	for (uint8_t i = 0; i < readed; i++) {
		LOGF("0x%"PRIx8" ", data[i]);
	}
	LOG(NEW_LINE);
#endif

	done:
	if (readed < len) {
		LOGF_LN("eeprom_read(): only readed %"PRIu8" out of %"PRIu8" bytes", readed, len);
	}

	return readed;
}
