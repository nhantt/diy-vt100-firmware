/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/gpio.h>
#include "led.h"

static const struct {
	uint32_t port;
	uint16_t pin;
} mux[] = {
	{GPIOC, GPIO4}, /* ONLINE */
	{GPIOC, GPIO5}, /* LOCAL */
	{GPIOB, GPIO2}, /* KBDLOCKED */
	{GPIOE, GPIO7}, /* L1 */
	{GPIOE, GPIO8}, /* L2 */
	{GPIOE, GPIO9}, /* L3 */
	{GPIOE, GPIO10} /* L4 */
};

/**
 * Setup the LED.
 */
void led_setup(void)
{
	for (unsigned i = 0; i < 7; i++) {
		gpio_clear(mux[i].port, mux[i].pin);
		gpio_set_output_options(mux[i].port, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, mux[i].pin);
		gpio_mode_setup(mux[i].port, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, mux[i].pin);
	}
}

void led_on(enum vt100_led leds)
{
	for (unsigned i = 0; i < 7; i++) {
		if (leds & (1 << i)) {
			GPIO_BSRR(mux[i].port) = mux[i].pin;
		}
	}
}

void led_off(enum vt100_led leds)
{
	for (unsigned i = 0; i < 7; i++) {
		if (leds & (1 << i)) {
			GPIO_BSRR(mux[i].port) = mux[i].pin << 16;
		}
	}
}
