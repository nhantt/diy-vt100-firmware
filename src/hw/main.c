/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/flash.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/cm3/scb.h>
#include <unicore-mx/cm3/systick.h>

#include "common.h"
#include "generic/cqueue.h"
#include "screen.h"
#include "hw.h"
#include "vt100.h"
#include "uart.h"
#include "generic/ps2-kbd.h"
#include "bell.h"
#include "eeprom.h"
#include "delay.h"

#include "state.h"
#include "vt100/misc.h"

#include "setup.h"
#include "debug.h"
#include "led.h"

/*
 * TIM1#
 * TIM2#
 * TIM3#
 * TIM4#
 * TIM5#
 * TIM6    DAC trigger
 * TIM7    Bell Duration maintainer
 * TIM8#   LCD backlight
 * TIM9#
 * TIM10   USB poll delay
 * TIM11   SETUP Button debouncing
 * TIM12   LCD cursor blink
 * TIM13   Delay function
 * TIM14#
 *
 * USART1#  WORLD connector
 * USART2#
 * USART3   WORLD connector (TX)
 * UART4#
 * UART5#
 * UART8    DEBUG connector
 *
 * I2C1#
 * I2C4#   Setting EEPROM
 *
 * CAN1#
 * HDMI-CEC#
 *
 * TIMn#, USARTn#, I2Cn# = Available on "EXT" connector
 */

void screen_refresh(void);

extern volatile struct cqueue uart_tx, uart_rx;
extern volatile struct cqueue ps2kbd;

static void __autox_feature(void);

void delay_init(void);
void debug_setup(void);
void led_setup(void);
void bell_setup(void);
void screen_setup(void);
void screen_splash(void);
void screen_main(void);
void uart_setup(void);
void ps2_setup(void);
void screen_setup(void);
void usb_setup(void);
void usb_poll(void);
void cursor_blink_poll(void);
void cursor_blink_setup(void);
void screen_brightness_setup(void);
void button_setup(void);
void button_poll(void);
void usb_enable(void);
void ps2_enable(void);
void touch_setup(void);
void touch_poll(void);

static const struct rcc_clock_scale rcc_hse_16mhz_3v3_out_48mhz = {
	.pllm = 16,
	.plln = 192,
	.pllp = 4,
	.pllq = 4,
	.hpre = RCC_CFGR_HPRE_DIV_NONE,
	.ppre1 = RCC_CFGR_PPRE_DIV_NONE,
	.ppre2 = RCC_CFGR_PPRE_DIV_NONE,
	.flash_config = FLASH_ACR_ICE | FLASH_ACR_DCE |
			FLASH_ACR_LATENCY_1WS,
	.ahb_frequency  = 48000000,
	.apb1_frequency = 48000000,
	.apb2_frequency = 48000000
};

static void lcd_clock(void)
{
	/*
	 * The datasheet says (Figure 12, page 150):
	 *     The LCD-TFT clock comes from PLLSAI.
	 *     PLLSRC selects either HSI or HSE.
	 *     PLLSAI's input clock is either HSI or HSE divided by PLLM.
	 *     PLLSAI's PLLLCDCLK output is the input * PLLSAIN / PLLSAIR.
	 *     LCD-TFT clock is PLLLCDCLK divided by PLLSAIDIVR.
	 *
	 * PLLSRC and PLLM are in the RCC_PLLCFGR register.
	 * PLLSAIN and PLLSAIR are in RCC_PLLSAICFGR.
	 * PLLSAIDIVR is in RCC_DCKCFGR1;
	 *
	 * In our case,
	 * PLLSRC already selected HSE, which is 16 MHz.
	 * PLLM is already set to 16.  16 MHz / 16 = 1 MHz.
	 * We set PLLSAIN = 208 and PLLSAIR = 4.  1 MHz * 208 / 4 = 52 MHz.
	 * We set PLLSAIDIVR to 2.  52 MHz / 2 = 26 MHz.
	 * So the LCD-TFT pixel clock is 26 MHz.
	 *
	 * The number of clocks per frame is
	 * (VSYNC + VBP + LCD_HEIGHT + VFP) * (HSYNC + HBP + LCD_WIDTH + HFP) =
	 * (1 + 23 + 480 + 7) * (1 + 46 + 800 + 1) = 433328.
	 *
	 * So the refresh frequency is 26 MHz / 433328 = ~60 Hz.
	 */

	RCC_PLLSAICFGR = (208 << RCC_PLLSAICFGR_PLLSAIN_SHIFT) |
					(4 << RCC_PLLSAICFGR_PLLSAIQ_SHIFT) |
					(4 << RCC_PLLSAICFGR_PLLSAIR_SHIFT);
	RCC_DCKCFGR1 |= RCC_DCKCFGR1_PLLSAIDIVR_DIVR_2;
	RCC_CR |= RCC_CR_PLLSAION;
}

/**
 * Initalize the clock.
 */
static inline void clock_setup(void)
{
	/* Base board frequency, set to 180MHz */
	rcc_clock_setup_hse_3v3(&rcc_hse_16mhz_3v3_out_48mhz);
	lcd_clock();

	/* Enable peripherals clock */
	rcc_periph_clock_enable(RCC_OTGHS);
	rcc_periph_clock_enable(RCC_GPIOA);
	rcc_periph_clock_enable(RCC_GPIOB);
	rcc_periph_clock_enable(RCC_GPIOC);
	rcc_periph_clock_enable(RCC_GPIOD);
	rcc_periph_clock_enable(RCC_GPIOE);
	rcc_periph_clock_enable(RCC_GPIOH);
	rcc_periph_clock_enable(RCC_DMA1);
	rcc_periph_clock_enable(RCC_TIM6);
	rcc_periph_clock_enable(RCC_TIM7);
	rcc_periph_clock_enable(RCC_TIM8);
	rcc_periph_clock_enable(RCC_TIM10);
	rcc_periph_clock_enable(RCC_TIM11);
	rcc_periph_clock_enable(RCC_TIM12);
	rcc_periph_clock_enable(RCC_TIM13);
	rcc_periph_clock_enable(RCC_I2C4);
	rcc_periph_clock_enable(RCC_USART1);
	rcc_periph_clock_enable(RCC_UART8);
	rcc_periph_clock_enable(RCC_LTDC);
	rcc_periph_clock_enable(RCC_DAC);
	rcc_periph_clock_enable(RCC_SYSCFG);
	rcc_periph_clock_enable(RCC_SPI3);
}

static void process_queue(volatile struct cqueue *queue, void (*cb)(uint8_t))
{
	while (CQUEUE_HAS_DATA(queue)) {
		cb(cqueue_pop(queue));
	}
}

/* Dummy */
void sys_tick_handler(void) {}

/* Used to wave up CPU every 1ms (ie 1KHz) @ 48MHz */
static void systick_setup(void)
{
	systick_set_reload(48000);
	systick_set_clocksource(STK_CSR_CLKSOURCE_AHB);
	systick_counter_enable();
	systick_interrupt_enable();
}

void main(void)
{
	clock_setup();
	uart_setup();
#if (DEBUG_BUILD == 1)
	debug_setup();
#endif
	led_setup();
	bell_setup();
	cursor_blink_setup();
	screen_setup();
	ps2_setup();
	usb_setup();
	screen_brightness_setup();
	eeprom_setup();
	button_setup();
	systick_setup();
	touch_setup();

	hw_setting_load();

#if (SHOW_SPLASH == 1)
	LOG_LN("Loading splash");
	hw_screen_brightness(60);
	screen_splash();
	led_on(VT100_ONLINE | VT100_LOCAL | VT100_KBDLOCKED |
				VT100_L1 | VT100_L2 | VT100_L3 | VT100_L4);

	delay(DELAY_RESOL_1MS, 1500); /* 1.5s */

	led_off(VT100_ONLINE | VT100_LOCAL | VT100_KBDLOCKED |
				VT100_L1 | VT100_L2 | VT100_L3 | VT100_L4);

	/* TODO: if EEPROM not available, give startup error bell */
#endif

	hw_bell(VT100_BELL_SHORT);
	LOG_LN("Hardware initalization finished");

	vt100_init();

	hw_screen_brightness(vt100.brightness);
	screen_invert(vt100.DECSCNM);
	uart_setting_update();

	screen_main();
	usb_enable();
	ps2_enable();

	for (;;) {
		usb_poll();

		process_queue(&ps2kbd, ps2kbd_decode);

		/* use AUTO XON */
		__autox_feature();

		if (!vt100.SETUP_SHOW) {
			/* Process data if only SETUP not being shown */
			process_queue(&uart_rx, vt100_state);
		}

		cursor_blink_poll();

		button_poll();

		touch_poll();

		screen_refresh();

		__asm__("wfi");
	}
}

static void __autox_feature(void)
{
	/* AUTOX is enabled */
	if (!vt100.AUTOX) {
		return;
	}

	/* we are half way in buffer */
	if (CQUEUE_HALF_FULL(&uart_rx)) {
		/* XOFF isnt send */
		if (!vt100.XOFF_SEND) {
			uart_send(ASCII_XOFF);
			vt100.XOFF_SEND = true;
			vt100.XOFF_SCROLL = false;
		}
	} else if (CQUEUE_EMPTY(&uart_rx)) {
		/* XON is send */
		if (vt100.XOFF_SEND) {
			uart_send(ASCII_XON);
			vt100.XOFF_SEND = false;
			vt100.XOFF_SCROLL = false;
		}
	}
}

extern unsigned _data_loadaddr, _data, _edata, _bss, _ebss;
extern unsigned _data_dtcm_loadaddr, _data_dtcm, _edata_dtcm, _bss_dtcm, _ebss_dtcm;
typedef void (*funcp_t) (void);
extern funcp_t __preinit_array_start, __preinit_array_end;
extern funcp_t __init_array_start, __init_array_end;
extern funcp_t __fini_array_start, __fini_array_end;

void reset_handler(void)
{
	volatile unsigned *src, *dest;
	funcp_t *fp;

	/* DTCM */

	for (src = &_data_dtcm_loadaddr, dest = &_data_dtcm;
		dest < &_edata_dtcm;
		src++, dest++) {
		*dest = *src;
	}

	for (dest = &_bss_dtcm; dest < &_ebss_dtcm; ) {
		*dest++ = 0;
	}

	/* SRAM1, SRAM2 */

	for (src = &_data_loadaddr, dest = &_data;
		dest < &_edata;
		src++, dest++) {
		*dest = *src;
	}

	for (dest = &_bss; dest < &_ebss; ) {
		*dest++ = 0;
	}

	/* Ensure 8-byte alignment of stack pointer on interrupts */
	/* Enabled by default on most Cortex-M parts, but not M3 r1 */
	SCB_CCR |= SCB_CCR_STKALIGN;

	/* Enable access to Floating-Point coprocessor. */
	SCB_CPACR |= SCB_CPACR_FULL * (SCB_CPACR_CP10 | SCB_CPACR_CP11);

	/* Constructors. */

	for (fp = &__preinit_array_start; fp < &__preinit_array_end; fp++) {
		(*fp)();
	}

	for (fp = &__init_array_start; fp < &__init_array_end; fp++) {
		(*fp)();
	}

	/* Call the application's entry point. */
	main();

	/* Destructors. */
	for (fp = &__fini_array_start; fp < &__fini_array_end; fp++) {
		(*fp)();
	}

}
