/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "common.h"
#include "misc.h"
#include <unicore-mx/cm3/scb.h>

/*
 * usuage of setting bits
 * vt100.HW_PRIV0 : cursor blink state
 * vt100.HW_PRIV1 : keyboard modifier
 * vt100.HW_PRIV2 : keyboard break
 * vt100.HW_PRIV3 : Cursor blink state toggled
 *
 * their maybe preprocessor directive to
 * replace decscriptive names with their crossponding HW_PRIV#
 */

void vt100_reset(void)
{
	scb_reset_system();
}

bool vt100_malfunctioning(void)
{
	return false;
}
