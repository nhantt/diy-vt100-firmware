/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "debug.h"
#include "hw.h"
#include "generic/cqueue.h"
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/exti.h>
#include <unicore-mx/cm3/nvic.h>

#define PS2_DATA_VAL   (!!(GPIOE_IDR & GPIO3))

void ps2_setup(void)
{
	/* Setup PS/2 pins. */
	gpio_mode_setup(GPIOE, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO2 | GPIO3);

	/* Configure the EXTI subsystem. */
	exti_select_source(EXTI2, GPIOE);
	exti_set_trigger(EXTI2, EXTI_TRIGGER_FALLING);
	exti_enable_request(EXTI2);

	/* Enable EXTI2 IRQ (note: shared with others) */
	nvic_enable_irq(NVIC_EXTI2_IRQ);

	/* Initalize the power supply */
	gpio_set(GPIOE, GPIO6);
	gpio_set_output_options(GPIOD, GPIO_OTYPE_OD, GPIO_OSPEED_2MHZ, GPIO6);
	gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO6);
}

void ps2_enable(void)
{
	gpio_clear(GPIOE, GPIO6);
}

static bool odd_parity(uint8_t data)
{
	data ^= data >> 4;
	data ^= data >> 2;
	data ^= data >> 1;
	return !(data & 0x1);
}

/* 64byte should be enought for PS2 keyboard (being used by human) */
#define BITS_USED 7

PLACE_IN_DTCM_BSS
static uint8_t ps2kbd_buf[CQUEUE_DATA_LEN(BITS_USED)];

PLACE_IN_DTCM_DATA
volatile struct cqueue ps2kbd = {
	.head = 0,
	.used = 0,
	.mod_mask = CQUEUE_MOD_MASK(BITS_USED),
	.data = ps2kbd_buf
};

void exti2_isr(void)
{
	EXTI_PR = EXTI2;

	if (GPIOE_IDR & GPIO2) {
		/* Noise? */
		return;
	}

	static uint8_t data = 0, index = 0;

	switch (index) {
	case 0:
		/* START bit */
		if (PS2_DATA_VAL) {
			LOG_LN("START bit not low, out of order?");
		}
	break;

	case 1:
	case 2:
	case 3:
	case 4:
	case 5:
	case 6:
	case 7:
	case 8:
		/* DATA bits */
		if (PS2_DATA_VAL) {
			data |= 1 << (index - 1);
		}
	break;

	case 9:
		/* PARITY bit */
		if (odd_parity(data) != PS2_DATA_VAL) {
			LOG_LN("parity check failed");
		}
	break;

	case 10:
		/* STOP bit */
		if (!PS2_DATA_VAL) {
			LOG_LN("STOP bit not high, out of order?");
		}

		LOGF_LN("GOT byte 0x%"PRIx8, data);
		cqueue_push(&ps2kbd, data);
	default:
		index = 0;
		data = 0;
		return;
	}

	index++;
}
