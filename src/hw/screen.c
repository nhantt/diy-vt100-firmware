/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <unicore-mx/stm32/timer.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/spi.h>
#include <unicore-mx/stm32/rcc.h>
#include <unicore-mx/stm32/ltdc.h>
#include <string.h>

#include "misc.h"
#include "screen.h"
#include "hw.h"
#include "screen.h"
#include "setup.h"
#include "vt100.h"
#include "uart.h"
#include "vt100/cursor.h"
#include "delay.h"
#include "debug.h"
#include "generic/bit-array.h"

/* TODO
 * - Accept answerback string from user
 */

#define  LCD_WIDTH               800  /* LCD pixel width            */
#define  LCD_HEIGHT              480  /* LCD pixel height           */
#define  LCD_HSYNC               1    /* Horizontal synchronization */
#define  LCD_HBP                 46   /* Horizontal back porch      */
#define  LCD_HFP                 16   /* Horizontal front porch     */
#define  LCD_VSYNC               1    /* Vertical synchronization   */
#define  LCD_VBP                 23   /* Vertical back porch        */
#define  LCD_VFP                 7    /* Vertical front porch       */

#define CURSOR_STATE HW_PRIV0
#define CURSOR_STATE_TOGGLED HW_PRIV3

static void lcd_config(void);
static void lcd_pinmux(void);
static void lcd_config_layer1(void);
static void lcd_config_layer2(void);

static uint8_t lcd_buf[SCREEN_HEIGHT][SCREEN_WIDTH];

void lcd_full_clear(void)
{
	memset(lcd_buf, 0, sizeof(lcd_buf));
}

void ltdc_pin(uint32_t port, uint32_t pins, uint32_t af)
{
	gpio_set_af(port, af, pins);
	gpio_set_output_options(port, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, pins);
	gpio_mode_setup(port, GPIO_MODE_AF, GPIO_PUPD_NONE, pins);
}

void lcd_pinmux(void)
{
	/*
	 * R2    ->  PA1   (AF14)
	 * B5    ->  PA3   (AF14)
	 * VSYNC ->  PA4   (AF14)
	 * G2    ->  PA6   (AF14)
	 * R6    ->  PA8   (AF14)
	 * R4    ->  PA11  (AF14)
	 * R5    ->  PA12  (AF14)
	 * R3    ->  PB0   (AF9)
	 * G7    ->  PB5   (AF14)
	 * B6    ->  PB8   (AF14)
	 * B7    ->  PB9   (AF14)
	 * G4    ->  PB10  (AF14)
	 * G5    ->  PB11  (AF14)
	 * HSYNC ->  PC6   (AF14)
	 * G6    ->  PC7   (AF14)
	 * B2    ->  PC9   (AF14)
	 * B3    ->  PD10  (AF14)
	 * DISP  ->  PD14  (OUTPUT OD)
	 * G3    ->  PE11  (AF14)
	 * B4    ->  PE12  (AF14)
	 * DE    ->  PE13  (AF14)
	 * CLK   ->  PE14  (AF14)
	 * R7    ->  PE15  (AF14)
	 */

	ltdc_pin(GPIOA, GPIO1 | GPIO3 | GPIO4 | GPIO6 | GPIO8 | GPIO11 | GPIO12, GPIO_AF14);
	ltdc_pin(GPIOB, GPIO0, GPIO_AF9);
	ltdc_pin(GPIOB, GPIO5 | GPIO8 | GPIO9 | GPIO10 | GPIO11, GPIO_AF14);
	ltdc_pin(GPIOC, GPIO6 | GPIO7 | GPIO9, GPIO_AF14);
	ltdc_pin(GPIOD, GPIO10, GPIO_AF14);
	ltdc_pin(GPIOE, GPIO11 | GPIO12 | GPIO13 | GPIO14 | GPIO15, GPIO_AF14);

	/* LCD_DISP GPIO configuration */
	gpio_set(GPIOD, GPIO14);
	gpio_set_output_options(GPIOD, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO14);
	gpio_mode_setup(GPIOD, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO14);
}

#if (SHOW_SPLASH == 1)

#define SPLASH_DATA_WIDTH 800
#define SPLASH_DATA_HEIGHT 480
extern uint8_t splash_data[SPLASH_DATA_HEIGHT][SPLASH_DATA_WIDTH];

#define SPLASH_CLUT_COUNT 256
extern uint32_t splash_clut[SPLASH_CLUT_COUNT];

void screen_splash(void)
{
	/* Windowing configuration */
	ltdc_setup_windowing(LTDC_LAYER_1, SPLASH_DATA_WIDTH, SPLASH_DATA_HEIGHT);

	/* Specifies the pixel format */
	ltdc_set_pixel_format(LTDC_LAYER_1, LTDC_LxPFCR_L8);

	/* Framebuffer address */
	ltdc_set_fbuffer_address(LTDC_LAYER_1, (uint32_t)splash_data);

	/* Configures the color frame buffer pitch in byte */
	ltdc_set_fb_line_length(LTDC_LAYER_1, SPLASH_DATA_WIDTH, SPLASH_DATA_WIDTH);

	/* Configures the frame buffer line number */
	ltdc_set_fb_line_count(LTDC_LAYER_1, SPLASH_DATA_HEIGHT);

	/* Enable layer 1 */
	ltdc_layer_ctrl_enable(LTDC_LAYER_1, LTDC_LxCR_LAYER_ENABLE | LTDC_LxCR_CLUT_ENABLE);

	for (unsigned i = 0; i < SPLASH_CLUT_COUNT; i++) {
		LTDC_L1CLUTWR = splash_clut[i];
	}

	/* Sets the Reload type */
	ltdc_reload(LTDC_SRCR_IMR);
}

#endif

static void lcd_config_layer1(void)
{
	/* Windowing configuration */
	ltdc_setup_windowing(LTDC_LAYER_1, SCREEN_WIDTH, SCREEN_HEIGHT);

	/* Specifies the pixel format */
	ltdc_set_pixel_format(LTDC_LAYER_1, LTDC_LxPFCR_L8);

	/* Framebuffer address */
	ltdc_set_fbuffer_address(LTDC_LAYER_1, (uint32_t)lcd_buf);

	/* Configures the color frame buffer pitch in byte */
	ltdc_set_fb_line_length(LTDC_LAYER_1, SCREEN_WIDTH, SCREEN_WIDTH);

	/* Configures the frame buffer line number */
	ltdc_set_fb_line_count(LTDC_LAYER_1, SCREEN_HEIGHT);

	/* Enable layer 1 */
	ltdc_layer_ctrl_enable(LTDC_LAYER_1, LTDC_LxCR_LAYER_ENABLE | LTDC_LxCR_CLUT_ENABLE);

	for (unsigned i = 0; i < 256; i++) {
		uint32_t color = (i << 24) | (i << 16) | (i << 8) | (i << 0);
		LTDC_L1CLUTWR = color;
	}

	/* Sets the Reload type */
	ltdc_reload(LTDC_SRCR_IMR);
}

static void lcd_config_layer2(void)
{
	/* Windowing configuration */
	ltdc_setup_windowing(LTDC_LAYER_2, SCREEN_WIDTH, SCREEN_HEIGHT);

	/* Specifies the pixel format */
	ltdc_set_pixel_format(LTDC_LAYER_2, LTDC_LxPFCR_L8);

	/* Framebuffer address */
	ltdc_set_fbuffer_address(LTDC_LAYER_2, (uint32_t)lcd_buf);

	/* Configures the color frame buffer pitch in byte */
	ltdc_set_fb_line_length(LTDC_LAYER_2, SCREEN_WIDTH, SCREEN_WIDTH);

	/* Configures the frame buffer line number */
	ltdc_set_fb_line_count(LTDC_LAYER_2, SCREEN_HEIGHT);

	/* Enable layer 1 */
	ltdc_layer_ctrl_enable(LTDC_LAYER_2, LTDC_LxCR_CLUT_ENABLE);

	for (unsigned i = 0; i < 256; i++) {
		uint32_t color = (i << 24) | (i << 16) | (i << 8) | (i << 0);
		LTDC_L2CLUTWR = color ^ 0xFFFFFF;
	}

	/* Sets the Reload type */
	ltdc_reload(LTDC_SRCR_IMR);
}

/**
  * @brief LCD Configuration.
  * @note  This function Configure tha LTDC peripheral :
  *        1) Configure the Pixel Clock for the LCD
  *        2) Configure the LTDC Timing and Polarity
  *        3) Configure the LTDC Layer 1 :
  *           - The frame buffer is located at FLASH memory
  *           - The Layer size configuration : 800x480
  * @retval
  *  None
  */
static void lcd_config(void)
{
	/* LTDC Initialization */
	ltdc_ctrl_disable(LTDC_GCR_HSPOL_ACTIVE_HIGH); /* Active Low Horizontal Sync */
	ltdc_ctrl_disable(LTDC_GCR_VSPOL_ACTIVE_HIGH); /* Active Low Vertical Sync */
	ltdc_ctrl_disable(LTDC_GCR_DEPOL_ACTIVE_HIGH); /* Active Low Date Enable */
	ltdc_ctrl_disable(LTDC_GCR_PCPOL_ACTIVE_HIGH); /* Active Low Pixel Clock */

	/* Configure the LTDC */
	ltdc_set_tft_sync_timings(LCD_HSYNC, LCD_VSYNC,
								LCD_HBP, LCD_VBP,
								LCD_WIDTH, LCD_HEIGHT,
								LCD_HFP, LCD_VFP);

	ltdc_set_background_color(0, 0, 0);
	ltdc_ctrl_enable(LTDC_GCR_LTDC_ENABLE);
}

void screen_main(void)
{
	/* Configure the Layer*/
	lcd_config_layer1();
	lcd_config_layer2();
}

void screen_setup(void)
{
	lcd_pinmux();
	lcd_config();
}

void screen_invert(bool invert)
{
	if (invert) {
		ltdc_layer_ctrl_disable(LTDC_LAYER_1, LTDC_LxCR_LAYER_ENABLE);
		ltdc_layer_ctrl_enable(LTDC_LAYER_2, LTDC_LxCR_LAYER_ENABLE);
		ltdc_set_background_color(0xFF, 0xFF, 0xFF);
	} else {
		ltdc_layer_ctrl_enable(LTDC_LAYER_1, LTDC_LxCR_LAYER_ENABLE);
		ltdc_layer_ctrl_disable(LTDC_LAYER_2, LTDC_LxCR_LAYER_ENABLE);
		ltdc_set_background_color(0, 0, 0);
	}

	ltdc_reload(LTDC_SRCR_IMR);
}

extern const uint8_t font_data[128][10];

static void write_char(uint8_t ch, vt100_row row, vt100_col col,
	bool draw_cursor, const struct _vt100_data_prop *data_prop,
	const struct _vt100_row_prop *row_prop)
{
	if (data_prop->char_set == VT100_CHAR_SPECIAL_GRAPHICS) {
		if (ch >= '_') {
			ch -= '_';
		}
	}

	const uint8_t *font_buf = font_data[ch];

	for (unsigned i = 0; i < 10; i++) { /* pixel row loop */
		unsigned i_mapped = i;
		if (row_prop->double_height) {
			i_mapped /= 2;
			i_mapped += row_prop->double_height_top ? 0 : 5;
		}

		for (unsigned j = 0; j < 10; j++) { /* pixel column loop */

			const static uint8_t bold_masks[10] = {
				1 << 0,
				3 << 0,
				3 << 1,
				3 << 2,
				3 << 3,
				3 << 4,
				3 << 5,
				3 << 6,

				/* strech the last 2 bit horizontally
				 * to connect horizontally */
				1 << 7,
				1 << 7,
			};

			const static uint8_t normal_masks[10] = {
				1 << 0,
				1 << 1,
				1 << 2,
				1 << 3,
				1 << 4,
				1 << 5,
				1 << 6,
				1 << 7,

				/* strech the last 2 bit horizontally
				 * to connect horizontally */
				1 << 7,
				1 << 7,
			};

			const uint8_t *masks = data_prop->bold ?
							bold_masks : normal_masks;
			uint8_t data = font_buf[i_mapped];
			uint8_t mask = masks[j];
			bool send = !!(data & mask);

			/* Underline */
			if (i_mapped == 9) {
				send = send || data_prop->underscore;
			}

			if (data_prop->blink) {
				/* Use cursor blink rate as text blink rate too */
				if (!vt100.CURSOR_STATE) {
					send = false;
				}
			}

			if (data_prop->invert) {
				send ^= true;
			}

			if (draw_cursor && vt100.CURSOR_STATE) {
				if (vt100.CURSOR) {
					/* block cursor */
					send ^= true;
				} else {
					/* underscore style */
					if (i_mapped == 9) {
						send ^= true;
					}
				}
			}

			unsigned px_row = (row * SCREEN_CHAR_HEIGHT) + i;
			unsigned px_col = (col * SCREEN_CHAR_WIDTH) + j;
			unsigned px_count = row_prop->double_width ? 2 : 1;
			px_col *= px_count;

			uint8_t px = send ? 0xFF : 0x00;
			while (px_count--) {
				lcd_buf[px_row][px_col + px_count] = px;
			}
		}
	}
}

static void touch_row(vt100_row row)
{
	row = ROW_MAP(row); /* to real */
	vt100.row_prop[row].redraw = true;
}

static const struct _vt100_row_prop all_reset_row_prop;

static const struct _vt100_data_prop all_reset_data_prop;

static const struct _vt100_data_prop invert_data_prop = {
	.invert = true
};

static const struct _vt100_data_prop bold_blink_data_prop = {
	.bold = true,
	.blink = true
};

static const struct _vt100_data_prop underscore_data_prop = {
	.underscore = true
};

static void screen_touch_row_with_blink_text(void)
{
	for (vt100_row i = 0; i < SCREEN_ROW; i++) {
		for (vt100_col j = 0; j < SCREEN_COL; j++) {
			if (vt100.buffer[i][j].prop.blink) {
				vt100.row_prop[i].redraw = true;
				break;
			}
		}
	}
}

static void setup_refresh(void)
{
	if (vt100.CURSOR_STATE_TOGGLED) {
		if (vt100.SETUP_TYPE) {
			setupB_refresh();
		} else {
			setupA_refresh();
		}

		vt100.CURSOR_STATE_TOGGLED = false;
	}
}

static void buffer_refresh(void)
{
	static struct _vt100_cursor last_cursor_pos = {0, 0};

	if (vt100.CURSOR_STATE_TOGGLED) {
		/* cursor state bit has toggled, we need to redraw the current row */
		touch_row(vt100.cursor.row);
		screen_touch_row_with_blink_text();
		vt100.CURSOR_STATE_TOGGLED = false;
	}

	if (last_cursor_pos.row != vt100.cursor.row) {
		/* row has changed, both need to be re-drawn */
		touch_row(last_cursor_pos.row);
		touch_row(vt100.cursor.row);
	} else if (last_cursor_pos.col != vt100.cursor.col) {
		/* column has changed, we need to redraw the current row */
		touch_row(last_cursor_pos.row);
	}

	last_cursor_pos = vt100.cursor;

	for (vt100_row i = 0; i < SCREEN_ROW; i++) {
		vt100_row row = ROW_MAP(i);

		struct _vt100_row_prop *row_prop = &vt100.row_prop[row];

		if (!row_prop->redraw) {
			continue;
		}

		vt100_col iter = SCREEN_COL / (row_prop->double_width ? 2 : 1);

		for (vt100_col j = 0; j < iter; j++) {
			bool under_cursor =
				(i == vt100.cursor.row) && (j == vt100.cursor.col);

			write_char(vt100.buffer[row][j].data, i, j, under_cursor,
				&vt100.buffer[row][j].prop, row_prop);
		}

		row_prop->redraw = false;
	}
}

void screen_refresh(void)
{
	if (vt100.SETUP_SHOW) {
		setup_refresh();
	} else {
		buffer_refresh();
	}
}

/* Brightness timer */
void screen_brightness_setup(void)
{
	TIM8_PSC = 4 - 1; /* clock divider, 12 MHz @ 48MHz (APB2) */
	TIM8_ARR = 100; /* frequency, 120 KHz  */
	TIM8_CCMR2 = TIM_CCMR2_CC3S_OUT | TIM_CCMR2_OC3M_PWM1; /* CH3 output PWM */
	TIM8_CCR3 = 0; /* duty cycle */
	TIM8_CCER = TIM_CCER_CC3E; /* enable CH3 */
	TIM8_BDTR = TIM_BDTR_MOE;
	TIM8_CR1 = TIM_CR1_CEN; /* contineously output */

	/* LCD_BL_EN GPIO configuration (TIM8_CH3) */
	gpio_set_af(GPIOC, GPIO_AF3, GPIO8);
	gpio_set_output_options(GPIOC, GPIO_OTYPE_PP, GPIO_OSPEED_2MHZ, GPIO8);
	gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO8);
}

/**
 * Set the screen brightness 0% to 100%
 * @param value a value in between 0-100
 */
void hw_screen_brightness(uint8_t value)
{
	LOGF_LN("Brightness = %"PRIu8, value);
	TIM8_CCR3 = value;
}

/**
 * blink cursor at 2hz
 */
void cursor_blink_setup(void)
{
	TIM12_PSC = 24000 - 1; /* 2KHz @ 48MHz (APB1)  */
	TIM12_ARR = 1000;
	TIM12_CR1 = TIM_CR1_CEN;
}

void cursor_blink_poll(void)
{
	if (TIM12_SR & TIM_SR_UIF) {
		TIM12_SR = 0;
		vt100.CURSOR_STATE ^= true;
		vt100.CURSOR_STATE_TOGGLED = true;
	}
}

/**
 * @param str NULL char terminaled string
 */
static void write_str(const char *str, vt100_row row, vt100_col col,
				const struct _vt100_data_prop *data_prop,
				const struct _vt100_row_prop *row_prop)
{
	for (vt100_col j = 0; str[j] != '\0'; j++) {
		write_char(str[j], row, col + j, false, data_prop, row_prop);
	}
}

/**
 * @a param type 'A' for SET-UP A or 'B' for SET-UP B
 */
static void print_setup_header(uint8_t type)
{
	static const struct _vt100_row_prop name_row_prop[] = {{
		.double_height = true,
		.double_height_top = true,
		.double_width = true,
		.redraw = true
	}, {
		.double_height = true,
		.double_height_top = false,
		.double_width = true,
		.redraw = true
	}};

	for (vt100_row i = 0; i < 2; i++) {
		write_str("SET-UP ", i, 0, &bold_blink_data_prop, &name_row_prop[i]);
		write_char(type, i, 7, false, &bold_blink_data_prop, &name_row_prop[i]);
	}

	/* The dedicated SETUP button is always advertised to user.
	 * Though still user can press F5 keyboard key to enter SETUP mode.
	 */
	write_str("TO EXIT PRESS \"SET-UP\"", 3, 0,
			&underscore_data_prop, &all_reset_row_prop);
}

static void print_speed(const char *name,
	const struct _vt100_uart_speed *speed, vt100_col col)
{
	vt100_row last_row = SCREEN_ROW - 1;

	write_str(name, last_row, col + 1,
		&all_reset_data_prop, &all_reset_row_prop);

	/* Note: Assuming that speed->bitrate string length will
	 *  be at maximum 6 character or less */
	char data[7];
	unsigned len = strlen(speed->bitrate);
	unsigned skip = 6 - len;
	memset(data, ' ', skip);
	memcpy(&data[skip], speed->bitrate, len);
	data[6] = '\0';

	/* print data */
	write_str(data, last_row, col + 8,
		&all_reset_data_prop, &all_reset_row_prop);
}

static void print_setupb_bit(vt100_col col, bool value)
{
	vt100_row last_row = SCREEN_ROW - 1;
	uint8_t data = value ? '1' : '0';

	write_char(data, last_row, col, false,
		&invert_data_prop, &all_reset_row_prop);
}

void setupB_refresh(void)
{
	vt100_row second_last_row = SCREEN_ROW - 2;
	vt100_row last_row = SCREEN_ROW - 1;

	print_setup_header('B');

	/* Write the cursor row */
	for (vt100_col j = 0; j < SCREEN_COL; j++) {
		bool under_cursor = (j == vt100.setup_col);
		write_char(' ', second_last_row, j, under_cursor,
			&all_reset_data_prop, &all_reset_row_prop);
	}

	print_speed("T SPEED", &vt100_uart_tx_speed[vt100.uart_tx], 49);
	print_speed("R SPEED", &vt100_uart_rx_speed[vt100.uart_rx], 65);

	write_char('1', last_row, 0, false,
		&all_reset_data_prop, &all_reset_row_prop);
	print_setupb_bit(2, vt100.DECSCLM);
	print_setupb_bit(3, vt100.DECARM);
	print_setupb_bit(4, vt100.DECSCNM);
	print_setupb_bit(5, vt100.CURSOR);

	write_char('2', last_row, 8, false,
		&all_reset_data_prop, &all_reset_row_prop);
	print_setupb_bit(10, vt100.MARGIN_BELL);
	print_setupb_bit(11, vt100.KEY_CLICK);
	print_setupb_bit(12, vt100.DECANM);
	print_setupb_bit(13, vt100.AUTOX);

	write_char('3', last_row, 16, false,
		&all_reset_data_prop, &all_reset_row_prop);
	print_setupb_bit(18, vt100.SHIFTED);
	print_setupb_bit(19, vt100.DECAWM);
	print_setupb_bit(20, vt100.LNM);
	print_setupb_bit(21, vt100.DECINLM);

	write_char('4', last_row, 24, false,
		&all_reset_data_prop, &all_reset_row_prop);
	print_setupb_bit(26, vt100.PARITY_SENSE);
	print_setupb_bit(27, vt100.PARITY);
	print_setupb_bit(28, vt100.BPC);
	print_setupb_bit(29, false); /* vt100.POWER */
}

void setupA_refresh(void)
{
	print_setup_header('A');

	vt100_row second_last_row = SCREEN_ROW - 2;
	vt100_row last_row = SCREEN_ROW - 1;
	static const char *num_row = "1234567890";

	for (vt100_col j = 0; j < SCREEN_COL; j++) {
		/* Write 'T' or ' ' */
		bool under_cursor = (j == vt100.setup_col);
		uint8_t data = bitarr_read(vt100.tab_stop, j) ? 'T' : ' ';
		write_char(data, second_last_row, j, under_cursor,
			&all_reset_data_prop, &all_reset_row_prop);

		/* Write number */
		uint8_t quo = j / 10;
		uint8_t rem = j - (quo * 10);
		const struct _vt100_data_prop *data_prop =
			(quo & 0x1) ? &invert_data_prop : &all_reset_data_prop;
		write_char(num_row[rem], last_row, j, false,
			data_prop, &all_reset_row_prop);
	}
}

void setup_show_wait(void)
{
	vt100_row first_row = 0;
	lcd_full_clear();

	write_str("Wait", first_row, 0,
		&all_reset_data_prop, &all_reset_row_prop);
}

/**
 * Called when setupA need to be shown
 */
void setupA_load(void)
{
	lcd_full_clear();
	setupA_refresh();
}

/**
 * Called when setupB need to be shown
 */
void setupB_load(void)
{
	lcd_full_clear();
	setupB_refresh();
}

/**
 * Called when setup need to be hidden
 */
void setup_exit(void)
{
	for (vt100_col j = 0; j < SCREEN_ROW; j++) {
		vt100.row_prop[j].redraw = true;
	}
}
