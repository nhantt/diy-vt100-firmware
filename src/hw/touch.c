/*
 * This file is part of diy-VT100.
 * Copyright (C) 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdbool.h>
#include <unicore-mx/stm32/gpio.h>
#include <unicore-mx/stm32/exti.h>
#include <unicore-mx/stm32/spi.h>
#include <unicore-mx/cm3/nvic.h>
#include "debug.h"

/* Control bits */
#define CTRL_START	(1 << 7) /* Start bit */
#define CTRL_A2		(1 << 6)
#define CTRL_A1		(1 << 5)
#define CTRL_A0		(1 << 4)
#define CTRL_MODE	(1 << 3) /* 1=8bit | 0=12bit */
#define CTRL_SER	(1 << 2) /* 1=Single ended | 0=Differential */

/* This should actually have been PUx
 *  since these keep the specific module powered up.
 * IC designers should be given lesson on code readability! ;)
 */
#define CTRL_PD1	(1 << 1) /* Keep REF on */
#define CTRL_PD0	(1 << 0) /* Keep ADC on */

#define CTRL_A_Y  (CTRL_A0)
#define CTRL_A_Z1 (CTRL_A1 | CTRL_A0)
#define CTRL_A_Z2 (CTRL_A2)
#define CTRL_A_X  (CTRL_A2 | CTRL_A0)

#define Z_THRESHOLD 25

void touch_setup(void)
{
	/* CS pin */
	gpio_set(GPIOE, GPIO0);
	gpio_set_output_options(GPIOE, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO0);
	gpio_mode_setup(GPIOE, GPIO_MODE_OUTPUT, GPIO_PUPD_NONE, GPIO0);

	/* SPI3 Pins (SCK, MISO, MOSI) */
	gpio_set_af(GPIOC, GPIO_AF6, GPIO10 | GPIO11 | GPIO12);
	gpio_set_output_options(GPIOC, GPIO_OTYPE_PP, GPIO_OSPEED_25MHZ, GPIO10 | GPIO11 | GPIO12);
	gpio_mode_setup(GPIOC, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO10 | GPIO11 | GPIO12);

	/* BUSY and PENIRQ pin */
	gpio_mode_setup(GPIOC, GPIO_MODE_INPUT, GPIO_PUPD_NONE, GPIO13 | GPIO14);

	/* PENIRQ EXTI configuration */
	exti_select_source(EXTI14, GPIOC);
	exti_set_trigger(EXTI14, EXTI_TRIGGER_FALLING);
	exti_reset_request(EXTI14);
	exti_enable_request(EXTI14);

	/* NVIC configuration */
	nvic_enable_irq(NVIC_EXTI15_10_IRQ);

	/* SPI3 Inialization
	 *  - SCK = 3MHz @ 48MHz
	 *  - Mode 0 (CPHA = 0, CPOL = 0)
	 *  - Full duplex communication
	 *  - Software slave management enabled
	 *  - MSB first
	 */
	SPI3_CR1 = SPI_CR1_MSTR | SPI_CR1_SSM | SPI_CR1_SSI |
				SPI_CR1_BAUDRATE_FPCLK_DIV_16 | SPI_CR1_SPE;
}

inline uint16_t transfer_payload16(uint8_t ctrl)
{
	uint8_t tmp __attribute__((unused));

	/* CS low */
	GPIOE_BSRR = GPIO0 << 16;

	/* 8bit communication and 8bit RX threshold */
	SPI3_CR2 = SPI_CR2_FRXTH | SPI_CR2_DS_8BIT;

	/* Wait for transfer finished. */
	while (!(SPI3_SR & SPI_SR_TXE));
	SPI3_DR8 = ctrl;

	/* Wait for transfer finished. */
	while (!(SPI3_SR & SPI_SR_RXNE));
	tmp = SPI3_DR8;

	/* 8bit communication and 16bit RX threshold */
	SPI3_CR2 = SPI_CR2_DS_8BIT;

	/* Wait till BUSY */
	//while (GPIOC_IDR & GPIO13);

	/* Write dummy data to read 16bit payload */
	/* while (!(SPI3_SR & SPI_SR_TXE)); */ /* Not required */
	SPI3_DR = 0x0000;

	/* Read data */
	while (!(SPI3_SR & SPI_SR_RXNE));

	/* CS high */
	GPIOE_BSRR = GPIO0;

	return SPI3_DR;
}

inline uint8_t transfer_payload8(uint8_t ctrl)
{
	uint8_t tmp __attribute__((unused));

	/* CS low */
	GPIOE_BSRR = GPIO0 << 16;

	/* 8bit communication and 8bit RX threshold */
	SPI3_CR2 = SPI_CR2_FRXTH | SPI_CR2_DS_8BIT;

	/* Wait for transfer finished. */
	while (!(SPI3_SR & SPI_SR_TXE));
	SPI3_DR8 = ctrl;

	/* Wait for transfer finished. */
	while (!(SPI3_SR & SPI_SR_RXNE));
	tmp = SPI3_DR8;

	/* Wait till busy */
	//while (GPIOC_IDR & GPIO13);

	/* Write dummy data to read 16bit payload */
	/* while (!(SPI3_SR & SPI_SR_TXE)); */ /* Not required */
	SPI3_DR8 = 0x00;

	/* Read data */
	while (!(SPI3_SR & SPI_SR_RXNE));

	/* CS high */
	GPIOE_BSRR = GPIO0;

	return SPI3_DR8;
}

/**
 * Process touch event
 */
void touch_process(void)
{
	/* Check if the touch press has crossed the specified threshold.
	 * As per datasheet, reading in 8bit mode is more than enough
	 * for pressure */
	uint8_t z1 = transfer_payload8(CTRL_START | CTRL_A_Z1 | CTRL_MODE | CTRL_PD1 | CTRL_PD0);
	uint8_t z2 = transfer_payload8(CTRL_START | CTRL_A_Z2 | CTRL_MODE | CTRL_PD1 | CTRL_PD0);

	LOGF_LN("Z1 = %"PRIu8", Z2 = %"PRIu8, z1, z2);

	if ((z2 - z1) < Z_THRESHOLD) {
		LOGF_LN("Press the screen harder! (touch threshold not reached)");
		transfer_payload8(CTRL_START | CTRL_MODE); /* PENIRQ enable */
		return;
	}

	/* Read coordinates */
	uint16_t x = transfer_payload16(CTRL_START | CTRL_A_X | CTRL_PD1 | CTRL_PD0) >> 4;
	uint16_t y = transfer_payload16(CTRL_START | CTRL_A_Y) >> 4; /* PENIRQ enable */

	LOGF_LN("Touch X = %"PRIu16", Y = %"PRIu16, x, y);
}

static volatile bool penirq = false;

void touch_poll(void)
{
	if (!penirq) {
		/* PENIRQ not triggered */
		return;
	}

	LOGF_LN("User has touch the screen");
	touch_process();
	penirq = false;
}

void exti15_10_isr(void)
{
	if (!(GPIOC_IDR & GPIO14)) {
		/* PENIRQ low */
		penirq = true;
	}

	EXTI_PR = EXTI14;
}
