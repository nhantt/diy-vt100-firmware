/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "hw.h"
#include "uart.h"
#include "generic/cqueue.h"
#include "../led.h"
#include "debug.h"

#include <unicore-mx/cm3/nvic.h>
#include <unicore-mx/stm32/usart.h>

/* TODO
 *  - 7bit and 8bit data size for UART
 *  - Seperate baud rate for RX and TX
 *  - Parity ON/OFF support
 *  - Partiy sense (ODD/EVEN) support
 */

#define TX_BITS_USE 13 /* 8KB */
#define RX_BITS_USE 13 /* 8KB */

PLACE_IN_DTCM_BSS
static uint8_t
	uart_tx_buf[CQUEUE_DATA_LEN(TX_BITS_USE)],
	uart_rx_buf[CQUEUE_DATA_LEN(RX_BITS_USE)];

PLACE_IN_DTCM_DATA
volatile struct cqueue uart_tx = {
	.head = 0,
	.used = 0,
	.mod_mask = CQUEUE_MOD_MASK(TX_BITS_USE),
	.data = uart_tx_buf
}, uart_rx = {
	.head = 0,
	.used = 0,
	.mod_mask = CQUEUE_MOD_MASK(RX_BITS_USE),
	.data = uart_rx_buf
};

void uart_setup(void)
{
	/* Setup GPIO pin */
	gpio_set_af(GPIOA, GPIO_AF7, GPIO9 | GPIO10);
	gpio_set_output_options(GPIOA, GPIO_OTYPE_PP, GPIO_OSPEED_100MHZ, GPIO9 | GPIO10);
	gpio_mode_setup(GPIOA, GPIO_MODE_AF, GPIO_PUPD_NONE, GPIO9 | GPIO10);

	/* Configure */
	usart_set_baudrate(USART1, UART_SPEED_DEFAULT);
	usart_set_databits(USART1, 8);
	usart_set_stopbits(USART1, USART_STOPBITS_1);
	usart_set_mode(USART1, USART_MODE_TX_RX);
	usart_set_parity(USART1, USART_PARITY_NONE);
	usart_set_flow_control(USART1, USART_FLOWCONTROL_NONE);
	USART1_CR1 |= USART_CR1_RXNEIE;

	/* Finally enable the USART. */
	usart_enable(USART1);

	/* Enable the USART1 interrupt. */
	nvic_enable_irq(NVIC_USART1_IRQ);
}

void usart1_isr(void)
{
	uint32_t sr = USART1_SR;

	/* Check if we were called because of RXNE. */
	if (sr & USART_SR_RXNE) {

		uint8_t data = USART1_RDR;
		if (sr & USART_SR_PE) {
			data = 176;
		}

		if (!CQUEUE_FULL(&uart_rx)) {
			/* problem, problem ... no more space left. */
			cqueue_push(&uart_rx, data);
		} else {
			/* TODO: "If the buffer overflows, the VT100 will begin to discard
			 * incoming characters and the error character will be displayed." */
		}

		/* We have readed the data */
		USART1_SR &= ~USART_SR_RXNE;
	}

	/* Check if we were called because of TXE. */
	if (sr & USART_SR_TXE) {
		/* TODO: send next data */
		if (CQUEUE_HAS_DATA(&uart_tx)) {
			USART1_TDR = cqueue_pop(&uart_tx);
		} else {
			/* Disable the TXE interrupt as we don't need it anymore. */
			USART1_CR1 &= ~USART_CR1_TXEIE;
		}
	}
}

/* TODO: make functionality so either keyboard
 *  keycode is send fully to host or nothing is sent.
 * No partial keycode should be sent to host */

void uart_send(uint8_t data)
{
	if (vt100.LOCAL) {
		/* software implementation of local mode */
		cqueue_push(&uart_rx, data);
		return;
	}

	if (CQUEUE_FULL(&uart_tx)) {
		LOG_LN("WARN: uart-tx cqueue is full (locking keyboard)");
		led_on(VT100_KBDLOCKED);
		vt100.KBD_LOCKED = true;
	} else {
		cqueue_push(&uart_tx, data);
		led_off(VT100_KBDLOCKED);
		vt100.KBD_LOCKED = false;
	}

	if (!vt100.XOFFED) {
		/* enable transmission */
		USART1_CR1 |= USART_CR1_TXEIE;
	}
}

void uart_loopback(bool enable)
{
	/* just ignore, no hardware based loopback! */
}

/* NOTE: this array should match directly to vt100_uart_speed array */
const uint32_t uart_baudrate[] = {
	1200, 1800, 2000, 2400, 3600, 4800, 9600, 14400, 19200, 28800, 38400,
	56000, 57600, 115200, 128000, 153600, 230400, 256000, 460800, 921600,
	0 /* End of list */
};

/**
 * @note since UART RX and TX has to be same
 */
void uart_setting_update(void)
{
	usart_set_baudrate(USART1, uart_baudrate[vt100.uart_rx]);
}

void setup_uart_tx_speed_changed(void)
{
	vt100.uart_rx = vt100.uart_tx;
}

void setup_uart_rx_speed_changed(void)
{
	vt100.uart_tx = vt100.uart_rx;
}
