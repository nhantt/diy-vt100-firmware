/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_LED_H
#define DIY_VT100_LED_H

#include "common.h"

__BEGIN_DECLS

enum vt100_led {
	VT100_ONLINE = 1 << 0,
	VT100_LOCAL = 1 << 1,
	VT100_KBDLOCKED = 1 << 2,
	VT100_L1 = 1 << 3,
	VT100_L2 = 1 << 4,
	VT100_L3 = 1 << 5,
	VT100_L4 = 1 << 6
};

void led_on(enum vt100_led leds);
void led_off(enum vt100_led leds);

__END_DECLS

#endif
