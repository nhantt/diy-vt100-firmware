/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_MISC_H
#define DIY_VT100_MISC_H

#include "common.h"

__BEGIN_DECLS

#include <stdbool.h>

/**
 * Perform a reset of the terminal.
 * This is used by generic code to restart the terminal.
 * This function is expected never to return.
 */
void vt100_reset(void) __attribute__((noreturn));

bool vt100_malfunctioning(void);

__END_DECLS

#endif
