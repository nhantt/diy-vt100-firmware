/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "screen.h"
#include "vt100/cursor.h"
#include "vt100.h"
#include "debug.h"
#include <string.h>

/* TODO: if screen support shift(down/up) in hardware,
 *  provide a infrastructure to do so.
 */

static void touch_all_rows(void)
{
	for (vt100_row i = 0; i < SCREEN_ROW; i++) {
		vt100.row_prop[i].redraw = true;
	}
}

/**
 * Zero out the row data.
 * @param row Row (Virtual row index, NOT real row)
 * @note Additionally force row to single height and single width.
 * @note DO NOT set the row redraw flag
 */
static void zero_the_row_data(vt100_row row)
{
	row = ROW_MAP(row); /* to real */

	memset(vt100.buffer[row], 0, sizeof(vt100.buffer[row]));
	vt100.row_prop[row].double_height = false;
	vt100.row_prop[row].double_width = false;
}

/**
 * Shift up the screen by 1
 */
void screen_shiftup(void)
{
	zero_the_row_data(0);
	vt100.row_map = ROW_MAP(1);
	touch_all_rows();
}

/**
 * Shift down the screen row by 1
 */
void screen_shiftdown(void)
{
	vt100.row_map = ROW_MAP(SCREEN_ROW - 1);
	zero_the_row_data(0);
	touch_all_rows();
}

/**
 * Increase screen brightness
 */
void screen_brightness_inc(void)
{
	if (vt100.brightness >= SCREEN_BRIGHTNESS_MAX) {
		return;
	}

	vt100.brightness += SCREEN_BRIGHTNESS_DELTA;
	hw_screen_brightness(vt100.brightness);
}

/**
 * Decrease screen brightness
 */
void screen_brightness_dec(void)
{
	if (vt100.brightness <= SCREEN_BRIGHTNESS_MIN) {
		return;
	}

	vt100.brightness -= SCREEN_BRIGHTNESS_DELTA;
	hw_screen_brightness(vt100.brightness);
}
