/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "setup.h"
#include "vt100.h"
#include "screen.h"
#include "vt100/misc.h"
#include "uart.h"
#include "misc.h"
#include "led.h"
#include "debug.h"
#include "generic/bit-array.h"

/**
 * To be called when user want to switch setup mode.
 */
void setup(void)
{
	vt100.SETUP_SHOW ^= true;

	uart_send(vt100.SETUP_SHOW ? ASCII_XOFF : ASCII_XON);
	vt100.XOFF_SEND = vt100.SETUP_SHOW;

	vt100.SETUP_TYPE = false;
	vt100.setup_col = 0;

	if (vt100.SETUP_SHOW) {
		LOG_LN("Entering SETUP");
		setupA_load();
	} else {
		LOG_LN("Exiting SETUP");
		vt100_refresh_connect_mode();
		uart_setting_update();
		setup_exit();
	}
}

void setup_switch(void)
{
	vt100.setup_col = 0;

	/* invert setting (A to B) or (B to A) */
	vt100.SETUP_TYPE ^= true;

	if (vt100.SETUP_TYPE) {
		LOG_LN("Switching to SETUP-B");
		setupB_load();
	} else {
		LOG_LN("Switching to SETUP-A");
		setupA_load();
	}
}

static void setup_cursor_change(vt100_row value)
{
	vt100.setup_col = value;

	if (vt100.SETUP_TYPE) {
		setupB_refresh();
	} else {
		setupA_refresh();
	}
}

void setup_cursor_first(void)
{
	setup_cursor_change(0);
}

void setup_cursor_prev(void)
{
	/* select left value */
	setup_cursor_change(MAX(vt100.setup_col - 1, 0));
}

void setup_cursor_next(void)
{
	/* select right value */
	setup_cursor_change(MIN(vt100.setup_col + 1, SCREEN_COL - 1));
}

void setup_cursor_tab(void)
{
	for (vt100_col j = vt100.setup_col + 1; j < SCREEN_COL; j++) {
		if (bitarr_read(vt100.tab_stop, j)) {
			setup_cursor_change(j);
			return;
		}
	}

	/* Goto last position */
	setup_cursor_change(SCREEN_COL - 1);
}

void setup_value_flip(void)
{
	/* flip values in setup, 5 was pressed */

	if (!vt100.SETUP_TYPE) {
		/* not in setup B */
		return;
	}

	switch (vt100.setup_col) {
	/* box 1 */
	case 2:
		vt100.DECSCLM ^= true;
	break;
	case 3:
		vt100.DECARM ^= true;
	break;
	case 4:
		vt100.DECSCNM ^= true;
		screen_invert(vt100.DECSCNM);
	break;
	case 5:
		vt100.CURSOR ^= true;
	break;

	/* box 2 */
	case 10:
		vt100.MARGIN_BELL ^= true;
	break;
	case 11:
		vt100.KEY_CLICK ^= true;
	break;
	case 12:
		vt100.DECANM ^= true;
	break;
	case 13:
		vt100.AUTOX ^= true;
	break;

	/* box 3 */
	case 18:
		vt100.SHIFTED ^= true;
	break;
	case 19:
		vt100.DECAWM ^= true;
	break;
	case 20:
		vt100.LNM ^= true;
	break;
	case 21:
		vt100.DECINLM ^= true;
	break;

	/* box 4 */
	case 26:
		vt100.PARITY_SENSE ^= true;
	break;
	case 27:
		vt100.PARITY ^= true;
	break;
	case 28:
		vt100.BPC ^= true;
	break;
	case 29:
		/* setting_flip(SETTING_POWER); */
	break;
	}

	setupB_refresh();
}

void setup_DECCOLM(void)
{
	/* is it setup A */
	if (!vt100.SETUP_TYPE) {
		vt100.DECCOLM ^= true;
	}
}

void setup_LOCAL(void)
{
	vt100.LOCAL ^= true;
}

void setup_TABS_clear(void)
{
	/* is it setup A */
	if (!vt100.SETUP_TYPE) {
		memset(vt100.tab_stop, 0, sizeof(vt100.tab_stop));
		setupA_refresh();
	}
}

void setup_TAB_flip(void)
{
	/* is it setup A */
	if (!vt100.SETUP_TYPE) {
		bitarr_flip(vt100.tab_stop, vt100.setup_col);
		setupA_refresh();
	}
}

void setup_uart_rx(void)
{
	if (!vt100.SETUP_TYPE) {
		return;
	}

	/* handle speeds */
	vt100.uart_rx++;
	if (vt100_uart_rx_speed[vt100.uart_rx].bitrate == NULL) {
		vt100.uart_rx = 0;
	}

	setup_uart_rx_speed_changed();

	/* speed of RX and TX can be same only */
	vt100.uart_tx = vt100.uart_rx;

	setupB_refresh();
}

void setup_uart_tx(void)
{
	if (!vt100.SETUP_TYPE) {
		return;
	}

	/* handle speeds */
	vt100.uart_tx++;
	if (vt100_uart_tx_speed[vt100.uart_tx].bitrate == NULL) {
		vt100.uart_tx = 0;
	}

	setup_uart_tx_speed_changed();

	/* in setupB */
	setupB_refresh();
}

void setup_recall(void)
{
	setup_show_wait();
	hw_setting_load();

	/* now switch to setupA */
	vt100.SETUP_TYPE = false;
	vt100.setup_col = 0;

	setupA_refresh();
}

void setup_save(void)
{
	setup_show_wait();
	hw_setting_store();

	/* now switch to setupA */
	vt100.SETUP_TYPE = false;
	vt100.setup_col = 0;

	setupA_refresh();
}
