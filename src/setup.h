/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_SETUP_H
#define DIY_VT100_SETUP_H

#include "common.h"

__BEGIN_DECLS

void setupA_load(void);
void setupA_refresh(void);

void setupB_load(void);
void setupB_refresh(void);

void setup_uart_tx_speed_changed(void);
void setup_uart_rx_speed_changed(void);

void setup(void);
void setup_switch(void);
void setup_recall(void);
void setup_save(void);
void setup_show_wait(void);
void setup_enter(void);
void setup_exit(void);

void setup_value_flip(void);
void setup_cursor_next(void);
void setup_cursor_prev(void);
void setup_cursor_first(void);
void setup_cursor_tab(void);

void setup_uart_tx(void);
void setup_uart_rx(void);

void setup_TAB_flip(void);
void setup_TABS_clear(void);

void setup_LOCAL(void);
void setup_DECCOLM(void);

void hw_setting_load(void);
void hw_setting_store(void);

__END_DECLS

#endif
