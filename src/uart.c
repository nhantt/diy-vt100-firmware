/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100.h"
#include "uart.h"
#include "vt100/attribute.h"
#include "screen.h"
#include "setup.h"

void uart_send_uint8(uint8_t val)
{
	uint8_t tmp = val / 100;

	if (tmp) {
		uart_send('0' + tmp);
		val -= tmp * 100;
	}

	tmp = val / 10;
	if (tmp) {
		uart_send('0' + tmp);
		val -= tmp * 10;
	}

	uart_send('0' + val);
}

void uart_send_string(const char *string)
{
	for (unsigned i = 0; string[i]; i++) {
		uart_send(string[i]);
	}
}

void uart_send_return(void)
{
	uart_send(ASCII_CR);

	if (vt100.LNM) {
		uart_send(ASCII_LF);
	}
}

static void __uart_send_keypad_mode(uint8_t ascii, uint8_t esc_seq)
{
	if (vt100.DECKPAM) {
		uart_send(ASCII_ESCAPE);
		uart_send(vt100.DECANM ? 'O' : '?');
		uart_send(esc_seq);
	} else {
		uart_send(ascii);
	}
}

void uart_send_keypad_num(uint8_t num)
{
	__uart_send_keypad_mode('0' + num, 'p' + num);
}

void uart_send_keypad_dot(void)
{
	__uart_send_keypad_mode('.', 'n');
}

void uart_send_keypad_dash(void)
{
	__uart_send_keypad_mode('-', 'm');
}

void uart_send_keypad_comma(void)
{
	__uart_send_keypad_mode(',', 'l');
}

void uart_send_keypad_enter(void)
{
	if (vt100.DECKPAM) {
		uart_send(ASCII_ESCAPE);
		uart_send(vt100.DECANM ? 'O' : '?');
		uart_send('M');
	} else {
		uart_send_return();
	}
}

void uart_send_keypad_pfn(uint8_t code)
{
	uart_send(ASCII_ESCAPE);

	if (vt100.DECANM) {
		uart_send('O');
	}

	uart_send(code);
}

void uart_send_arrow(uint8_t code)
{
	uart_send(ASCII_ESCAPE);

	if (vt100.DECANM) {
		uart_send(vt100.DECCKM ? 'O' : '[');
	}

	uart_send(code);
}

void uart_send_answerback_string(void)
{
	for (unsigned i = 0; i < ANSWERBACK_SIZE && vt100.answerback[i]; i++) {
		uart_send(vt100.answerback[i]);
	}
}
