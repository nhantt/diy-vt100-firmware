/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef DIY_VT100_UART_H
#define DIY_VT100_UART_H

#include "common.h"

__END_DECLS

#include "vt100.h"

struct _vt100_uart_speed {
	char *bitrate; /* NULL means end of list */
	char *value;
	char clkmul;
};

extern const struct _vt100_uart_speed *vt100_uart_rx_speed;
extern const struct _vt100_uart_speed *vt100_uart_tx_speed;

void uart_loopback(bool enable);

void uart_send(uint8_t data);
void uart_send_uint8(uint8_t val);
void uart_send_string(const char *string);

void uart_send_return(void);
void uart_send_answerback_string(void);

/**
 * Update the UART configuration
 *  - speed
 *  - bit per character
 *  - parity
 */
void uart_setting_update(void);

/* === KEYPAD === */

void uart_send_keypad_enter(void);
void uart_send_keypad_comma(void);
void uart_send_keypad_dash(void);
void uart_send_keypad_dot(void);
void uart_send_keypad_num(uint8_t num);

#define KEYPAD_PF1 'P'
#define KEYPAD_PF2 'Q'
#define KEYPAD_PF3 'R'
#define KEYPAD_PF4 'S'

void uart_send_keypad_pfn(uint8_t code);

/* === ARROW === */

#define ARROW_UP			'A'
#define ARROW_DOWN		'B'
#define ARROW_RIGHT		'C'
#define ARROW_LEFT		'D'

void uart_send_arrow(uint8_t code);

__END_DECLS

#endif
