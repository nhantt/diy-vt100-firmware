/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100/attribute.h"
#include "vt100/buffer.h"
#include "vt100/cursor.h"
#include "vt100.h"
#include "vt100/tab.h"
#include "state.h"

/**
 * DECDHL -- Double Height Line (DEC Private)
 *
 * Top Half: ESC # 3
 * Bottom Half: ESC # 4
 *
 * These sequences cause the line containing the active position to become the
 * top or bottom half of a double-height double-width line. The sequences must
 * be used in pairs on adjacent lines and the same character output must be sent
 * to both lines to form full double-height characters. If the line was
 * single-width single-height, all characters to the right of the center of the
 * screen are lost. The cursor remains over the same character position unless it
 * would be to the right of the right margin, in which case it is moved to the
 * right margin.
 *
 * NOTE: The use of double-width characters reduces the number of characters per
 * line by half.
 */

/**
 * @param top true for Top, false for bottom
 */
void vt100_DECDHL(bool top)
{
	vt100_row row = ROW_MAP(vt100.cursor.row);
	struct _vt100_row_prop *row_prop = &vt100.row_prop[row];

	row_prop->double_width = true;
	row_prop->double_height = true;
	row_prop->double_height_top = top;
	row_prop->redraw = true;

	/* TODO: Check and move the cursor to right margin if required */
}

/**
 * DECDWL -- Double-Width Line (DEC Private)
 *
 * ESC # 6
 *
 * This causes the line that contains the active position to become
 * double-width single-height. If the line was single-width single-height,
 * all characters to the right of the screen are lost. The cursor
 * remains over the same character position unless it would be to the
 * right of the right margin, in which case, it is moved to the right margin.
 *
 * NOTE: The use of double-width characters reduces the number of
 * characters per line by half.
 */
void vt100_DECDWL(void)
{
	vt100_row row = ROW_MAP(vt100.cursor.row);
	struct _vt100_row_prop *row_prop = &vt100.row_prop[row];

	row_prop->double_height = false;
	row_prop->double_width = true;
	row_prop->redraw = true;

	/* TODO: Check and move the cursor to right margin if required */
}

/**
 * DECSWL -- Single-width Line (DEC Private)
 *
 * ESC # 5
 *
 * This causes the line which contains the active position to become
 * single-width single-height. The cursor remains on the same character
 * position. This is the default condition for all new lines on the screen.
 */
void vt100_DECSWL(void)
{
	vt100_row row = ROW_MAP(vt100.cursor.row);
	struct _vt100_row_prop *row_prop = &vt100.row_prop[row];

	row_prop->double_height = false;
	row_prop->double_width = false;
	row_prop->redraw = true;
}

/**
 * SGR -- Select Graphic Rendition  [Format Effector]
 *
 * ESC [ Ps ; . . . ; Ps m        default value: 0
 *
 * Invoke the graphic rendition specified by the parameter(s). All
 * following characters transmitted to the VT100 are rendered according
 * to the parameter(s) until the next occurrence of SGR.
 *
 * Parameter    Parameter Meaning
 *    0         Attributes off
 *    1         Bold or increased intensity
 *    4         Underscore
 *    5         Blink
 *    7         Negative (reverse) image
 *
 * All other parameter values are ignored.
 *
 * Without the Advanced Video Option, only one type of character
 * attribute is possible as determined by the cursor selection; in that
 * case specifying either the underscore or the reverse attribute will
 * activate the currently selected attribute. (See cursor selection).
 */
void vt100_SGR(void)
{
	if (!vt100.param.count) {
		vt100.param.data[0] = 0;
		vt100.param.count = 1;
	}

	/* TODO: make changes for (AVO_INSTALLED == 0) here and in hw drawing */

	for (unsigned i = 0; i < vt100.param.count; i++) {
		switch (vt100.param.data[i]) {
#if (AVO_INSTALLED == 1)
		case 0:
			vt100.data_prop.bold = false;
			vt100.data_prop.invert = false;
			vt100.data_prop.blink = false;
			vt100.data_prop.underscore = false;
		break;
		case 1:
			vt100.data_prop.bold = true;
		break;
		case 4:
			vt100.data_prop.underscore = true;
		break;
		case 5:
			vt100.data_prop.blink = true;
		break;
		case 7:
			vt100.data_prop.invert = true;
		break;
#else /* (AVO_INSTALLED == 1) */
		case 0:
			vt100.data_prop = false;
		break;
		case 4:
		case 7:
			vt100.data_prop = true;
		break;
#endif
		}
	}
}
