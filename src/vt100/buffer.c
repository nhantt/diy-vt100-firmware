/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "vt100/buffer.h"
#include "bell.h"
#include "state.h"
#include "vt100.h"
#include "vt100/tab.h"
#include "vt100/attribute.h"
#include "vt100/cursor.h"

/**
 * Place @a data on screen.
 * Used when something need to be printed on screen.
 * If a control character (less 0x20 - SPACE character) is place, it
 *  instead print the caret notation of the control character
 * @param data Data to be placed on screen
 */
/* TODO: partial support for autowarp */
void vt100_putch(uint8_t data)
{
	/* screen function have no tension of screen overflow */
	vt100_row row = ROW_MAP(vt100.cursor.row);
	vt100_col col = vt100.cursor.col;

	/* save data */
	vt100.buffer[row][col].data = data;
	vt100.buffer[row][col].prop = vt100.data_prop;

	/* mark as to be printed */
	vt100.row_prop[row].redraw = true;

	/* increment position */
	vt100.cursor.col++;

	if (vt100.DECAWM) {
		/* screen overflow on right */
		if (vt100.cursor.col > vt100.margin.right) {
			vt100.cursor.col = vt100.margin.left;
			vt100.cursor.row++;
		}

		/* screen overflowed on bottom */
		if (vt100.cursor.row > vt100.margin.bottom) {
			screen_shiftup();

			vt100.cursor.row = vt100.margin.bottom;
			vt100.cursor.col = vt100.margin.left;
		}
	} else if (vt100.cursor.col > vt100.margin.right) {
		vt100.cursor.col = vt100.margin.right;
	}

	bell_margin();
}

/**
 * DECALN -- Screen Alignment Display (DEC Private)
 *
 * ESC # 8
 *
 * This command fills the entire screen area with uppercase Es for screen
 * focus and alignment. This command is used by DEC manufacturing and
 * Field Service personnel.
 */
void vt100_DECALN(void)
{
	vt100.row_map = 0;

	static const struct _vt100_row_prop cleared = { .redraw = true };

	static const struct _vt100_screen_char _E = {'E'};

	for (vt100_row i = 0; i < SCREEN_ROW; i++) {
		for (vt100_col j = 0; j < SCREEN_COL; j++) {
			vt100.buffer[i][j] = _E;
		}

		vt100.row_prop[i] = cleared;
	}
}

/**
 * Zero out the row data.
 * Additionally, force the row to single width.
 * @param row Row (Virtual row index, NOT real row)
 */
static void ED_zero_the_row_data(vt100_row row)
{
	row = ROW_MAP(row); /* to real */

	memset(vt100.buffer[row], 0, sizeof(vt100.buffer[row]));

	vt100.row_prop[row].double_height = false;
	vt100.row_prop[row].double_width = false;
	vt100.row_prop[row].redraw = true;
}

/**
 * Zero out the rows data.
 * Additionally, force the row to single width.
 * @param start Start Row (Virtual row index, NOT real row)
 * @param end End Row (Virtual row index, NOT real row) (inclusive)
 */
static void ED_zero_the_rows_data(vt100_row start, vt100_row end)
{
	for (vt100_row i = start; i <= end; i++) {
		vt100_row row = ROW_MAP(i); /* to real */

		memset(vt100.buffer[row], 0, sizeof(vt100.buffer[row]));

		vt100.row_prop[row].double_height = false;
		vt100.row_prop[row].double_width = false;
		vt100.row_prop[row].redraw = true;
	}
}

/**
 * Zero out a segment of the screen row buffer
 * @param row Row (Virtual row index, NOT real row)
 * @param start Start
 * @param end End (inclusive)
 */
static void EL_zero_the_row_segment_data(vt100_row row,
			vt100_col start, vt100_col end)
{
	row = ROW_MAP(row); /* to real */

	memset(&vt100.buffer[row][start], 0, sizeof(**vt100.buffer) * (end - start + 1));

	vt100.row_prop[row].redraw = true;
}

/**
 * Zero out the whole screen.
 * Additionally, force all the row to single width.
 */
static void ED_zero_the_screen_data(void)
{
	vt100.row_map = 0;

	memset(vt100.buffer, 0, sizeof(vt100.buffer));

	for (vt100_row i = 0; i < SCREEN_ROW; i++) {
		vt100.row_prop[i].double_height = false;
		vt100.row_prop[i].double_width = false;
		vt100.row_prop[i].redraw = true;
	}
}

/**
 * ED -- Erase In Display  [Editor Function]
 *
 * ESC [ Ps J         default value: 0
 *
 * This sequence erases some or all of the characters in the display according
 * to the parameter. Any complete line erased by this sequence will return that
 * line to single width mode.
 *
 * Parameter  Parameter Meaning
 *     0      Erase from the active position to the end of the screen,
 *            inclusive (default)
 *
 *     1      Erase from start of the screen to the active position, inclusive
 *
 *     2      Erase all of the display -- all lines are erased, changed to
 *            single-width, and the cursor does not move.
 */
void vt100_ED(void)
{
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 0;

	switch (v) {
	case 0:
		/* Clear from current position to end of screen */
		if (!vt100.cursor.col) {
			ED_zero_the_row_data(vt100.cursor.row);
		} else {
			EL_zero_the_row_segment_data(vt100.cursor.row,
					vt100.cursor.col, SCREEN_COL - 1);
		}

		if (vt100.cursor.row < (SCREEN_ROW - 1)) {
			ED_zero_the_rows_data(vt100.cursor.row + 1, SCREEN_ROW - 1);
		}
	break;
	case 1:
		/* Clear from top of screen to current position */
		if (vt100.cursor.row) {
			ED_zero_the_rows_data(0, vt100.cursor.row - 1);
		}

		if (vt100.cursor.col >= (SCREEN_COL - 1)) {
			ED_zero_the_row_data(vt100.cursor.row);
		} else {
			EL_zero_the_row_segment_data(vt100.cursor.row,
				vt100.cursor.col, SCREEN_COL - 1);
		}
	break;
	case 2:
		ED_zero_the_screen_data();
	break;
	}
}

/**
 * EL -- Erase In Line  [Editor Function]
 *
 * ESC [ Ps K          default value: 0
 *
 * Erases some or all characters in the active line according to the parameter.
 *
 * Parameter    Parameter Meaning
 *
 *    0         Erase from the active position to the end of the line,
 *              inclusive (default)
 *
 *    1         Erase from the start of the screen to the active position,
 *              inclusive
 *
 *    2         Erase all of the line, inclusive
 */
void vt100_EL(void)
{
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 0;

	switch (v) {
	case 0:
		/* current position to end of line */
		EL_zero_the_row_segment_data(vt100.cursor.row,
									vt100.cursor.col, SCREEN_COL - 1);
	break;
	case 1:
		/* start to line to current position */
		EL_zero_the_row_segment_data(vt100.cursor.row, 0, vt100.cursor.col);
	break;
	case 2:
		/* clear line */
		EL_zero_the_row_segment_data(vt100.cursor.row, 0, SCREEN_COL - 1);
	break;
	}
}

/**
 * LF -- Line Feed
 * This code causes a line feed or a new line operation. (See new line mode).
 */
void vt100_LF(void)
{
	vt100.cursor.row++;

	if (vt100.cursor.row > vt100.margin.bottom) {
		screen_shiftup();
		vt100.cursor.row = vt100.margin.bottom;
	}

	if (vt100.LNM) {
		vt100.cursor.col = vt100.margin.left;
		bell_margin();
	}
}

/**
 * CR -- Carrage Return
 *
 * Move cursor to left margin on the current line.
 */
void vt100_CR(void)
{
	vt100.cursor.col = vt100.margin.left;
	bell_margin();
}

/**
 * NEL -- Next Line  [Format Effector]
 *
 * ESC E
 *
 * This sequence causes the active position to move to the first position on
 * the next line downward. If the active position is at the bottom margin,
 * a scroll up is performed.
 */
void vt100_NEL(void)
{
	vt100.cursor.row++;

	if (vt100.cursor.row > vt100.margin.bottom) {
		screen_shiftup();
		vt100.cursor.row = vt100.margin.bottom;
	}

	vt100.cursor.col = vt100.margin.left;
	bell_margin();
}
