/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016, 2017 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100/tab.h"
#include "state.h"
#include "bell.h"
#include "vt100/cursor.h"

/**
 * BS -- Back Space
 *
 * Move the cursor to the left one character position,
 * unless it is at the left margin, in which case no action occurs.
 */
void vt100_BS(void)
{
	int col = vt100.cursor.col - 1;
	vt100.cursor.col = MAX(col, vt100.margin.left);
	bell_margin();
}

/**
 * CUB -- Cursor Backward [Editor Function]
 *
 * ESC [ Pn D          default value: 1
 *
 * The CUB sequence moves the active position to the left.
 * The distance moved is determined by the parameter.
 * If the parameter value is zero or one, the active position is moved one
 * position to the left. If the parameter value is n, the active position is
 * moved n positions to the left. If an attempt is made to move the cursor to
 * the left of the left margin, the cursor stops at the left margin.
 */
void vt100_CUB(void)
{
	/* TODO: margin are not supported */
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 1;
	int col = vt100.cursor.col - MAX(v, 1);
	vt100.cursor.col = MAX(col, vt100.margin.left);
	bell_margin();
}

/**
 * CUF -- Cursor Forward  [Editor Function]
 *
 * ESC [ Pn C          default value: 1
 *
 * The CUF sequence moves the active position to the right.
 * The distance moved is determined by the parameter.
 * A parameter value of zero or one moves the active position one position
 * to the right. A parameter value of n moves the active position n positions
 * to the right. If an attempt is made to move the cursor to the right of the
 * right margin, the cursor stops at the right margin.
 */
void vt100_CUF(void)
{
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 1;
	int col = vt100.cursor.col + MAX(v, 1);
	vt100.cursor.col = MIN(col, vt100.margin.right);
	bell_margin();
}

/**
 * CUU -- Cursor Up  [Editor Function]
 *
 * ESC [ Pn A          default value: 1
 *
 * Moves the active position upward without altering the column position.
 * The number of lines moved is determined by the parameter.
 * A parameter value of zero or one moves the active position one line upward.
 * A parameter value of n moves the active position n lines upward.
 * If an attempt is made to move the cursor above the top margin, the
 * cursor stops at the top margin.
 */
void vt100_CUU(void)
{
	/* TODO: margin are not supported | scroll up are not supported */
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 1;
	int row = vt100.cursor.row - MAX(v, 1);
	vt100.cursor.row = MAX(row, vt100.margin.top);
}

/**
 * CUD -- Cursor Down  [Editor Function]
 *
 * ESC [ Pn B          default value: 1
 *
 * The CUD sequence moves the active position downward without altering the
 * column position. The number of lines moved is determined by the parameter.
 * If the parameter value is zero or one, the active position is moved one line
 * downward. If the parameter value is n, the active position is moved n
 * lines downward. In an attempt is made to move the cursor below the
 * bottom margin, the cursor stops at the bottom margin.
 */
void vt100_CUD(void)
{
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 1;
	vt100_row row = vt100.cursor.row + MAX(v, 1);
	vt100.cursor.row = MIN(row, vt100.margin.bottom);
}

/**
 * CUP -- Cursor Position [Editor Function]
 *
 * ESC [ Pn ; Pn H          default value: 1
 *
 * The CUP sequence moves the active position to the position specified by the
 * parameters. This sequence has two parameter values, the first specifying the
 * line position and the second specifying the column position.
 * A parameter value of zero or one for the first or second parameter moves the
 * active position to the first line or column in the display, respectively.
 * The default condition with no parameters present is equivalent to a cursor to
 * home action. In the VT100, this control behaves identically with its format
 * effector counterpart, HVP.
 * The numbering of lines depends on the state of the Origin Mode (DECOM).
 *
 *
 *
 * HVP -- Horizontal and Vertical Position [Format Effector]
 *
 * ESC [ Pn ; Pn f          default value: 1
 *
 * Moves the active position to the position specified by the parameters.
 * This sequence has two parameter values, the first specifying the line
 * position and the second specifying the column. A parameter value of
 * either zero or one causes the active position to move to the first
 * line or column in the display, respectively. The default condition
 * with no parameters present moves the active position to the home position.
 * In the VT100, this control behaves identically with its editor
 * function counterpart, CUP. The numbering of lines and columns depends
 * on the reset or set state of the origin mode (DECOM).
 */
void vt100_CUP(void)
{
	uint8_t r = (vt100.param.count > 0) ? vt100.param.data[0] : 1;
	uint8_t c = (vt100.param.count > 1) ? vt100.param.data[1] : 1;

	/* convert: 1 indexed to 0 indexed */
	if (r) { r--; }
	if (c) { c--; }

	/* Parameter are relative to Left and top margin (ORIGIN) */
	if (vt100.DECOM) {
		r += vt100.margin.top;
		c += vt100.margin.left;
	}

	/* Limit to maximum row and column */
	r = MIN(r, SCREEN_ROW - 1);
	c = MIN(c, SCREEN_COL - 1);

	vt100.cursor.row = r;
	vt100.cursor.col = c;
	bell_margin();
}

PLACE_IN_DTCM_BSS
struct {
	struct _vt100_cursor cursor;

	enum _vt100_char_set G0:3, G1:3;
	uint8_t CHAR_SET_SO:1;

#if (AVO_INSTALLED == 1)
	struct _vt100_data_prop data_prop;
#else /* (AVO_INSTALLED == 1) */
	uint8_t data_prop:1;
	enum _vt100_char_set char_set:3;
#endif /* (AVO_INSTALLED == 1) */
} static backup;

/**
 * DECRC -- Restore Cursor (DEC Private)
 *
 * ESC 8
 *
 * This sequence causes the previously saved cursor position, graphic
 *  rendition, and character set to be restored.
 */
void vt100_DECRC(void)
{
	/* TODO: what to do if DECSC was never called? */
	vt100.cursor = backup.cursor;
	vt100.data_prop = backup.data_prop;
	vt100.G0 = backup.G0;
	vt100.G1 = backup.G1;
	vt100.CHAR_SET_SO = backup.CHAR_SET_SO;

#if (AVO_INSTALLED == 0)
	vt100.char_set = backup.char_set;
#endif /* (AVO_INSTALLED == 0) */
}

/**
 * DECSC -- Save Cursor (DEC Private)
 *
 * ESC 7
 *
 * This sequence causes the cursor position, graphic rendition, and character
 * set to be saved.
 */
void vt100_DECSC(void)
{
	backup.cursor = vt100.cursor;

	backup.data_prop = vt100.data_prop;
	backup.G0 = vt100.G0;
	backup.G1 = vt100.G1;
	backup.CHAR_SET_SO = vt100.CHAR_SET_SO;

#if (AVO_INSTALLED == 0)
	backup.char_set = vt100.char_set;
#endif /* (AVO_INSTALLED == 0) */
}

/**
 * IND -- Index  [Format Effector]
 *
 * ESC D
 *
 * This sequence causes the active position to move downward one line without
 * changing the column position. If the active position is at the bottom margin,
 * a scroll up is performed.
 */
void vt100_IND(void)
{
	vt100.cursor.row++;

	if (vt100.cursor.row > vt100.margin.bottom) {
		vt100.cursor.row = vt100.margin.bottom;
		screen_shiftup();
	}
}

/**
 * RI -- Reverse Index  [Format Effector]
 *
 * ESC M
 *
 * Move the active position to the same horizontal position on the preceding line.
 * If the active position is at the top margin, a scroll down is performed.
 */
void vt100_RI(void)
{
	if (vt100.cursor.row <= vt100.margin.top) {
		screen_shiftdown();
	} else {
		vt100.cursor.row--;
	}
}
