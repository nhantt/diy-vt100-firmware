/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100/margin.h"
#include "vt100.h"
#include "state.h"
#include "debug.h"

/* FIXME: Only margin set is possible,
 *  scrolling is still not supported by throughout the code */

/**
 * DECSTBM -- Set Top and Bottom Margins (DEC Private)
 *
 * ESC [ Pn ; Pn r        default values: see below
 *
 * This sequence sets the top and bottom margins to define the scrolling region.
 * The first parameter is the line number of the first line in the scrolling
 * region; the second parameter is the line number of the bottom line in the
 * scrolling region. Default is the entire screen (no margins). The minimum size
 * of the scrolling region allowed is two lines, i.e., the top margin must be
 * less than the bottom margin. The cursor is placed in the home position
 * (see Origin Mode DECOM).
 *
 * ---
 *
 * Scrolling Region
 *
 * ESC [ Pt ; Pb r
 *
 * Pt is the number of the top line of the scrolling region;
 * Pb is the number of the bottom line of the scrolling region and
 *  must be greater than Pt.
 */
void vt100_DECSTBM(void)
{
	uint8_t t = (vt100.param.count > 0) ? vt100.param.data[0] : 1;
	uint8_t b = (vt100.param.count > 1) ? vt100.param.data[1] : SCREEN_ROW;

	if (t >= b) {
		LOGF_LN("DECSTBM: top margin [%"PRIu8"] not less than "
			"bottom margin [%"PRIu8"]", t, b);
		return;
	}

	if (b > SCREEN_ROW) {
		LOGF_LN("DECSTBM: bottom margin greater than maximum rows, clipping");
		b = SCREEN_ROW;
	}

	vt100.margin.top = t ? (t - 1) : 0;
	vt100.margin.bottom = b ? (b - 1) : 0; /* Zero check not required */

	/* Move the cursor to home position */
	if (vt100.DECOM) {
		vt100.cursor.col = vt100.margin.left;
		vt100.cursor.row = vt100.margin.top;
	} else {
		vt100.cursor.col = 0;
		vt100.cursor.row = 0;
	}
}
