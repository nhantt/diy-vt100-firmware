/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100/misc.h"
#include "vt100/tab.h"
#include "vt100/buffer.h"
#include "state.h"
#include "../misc.h"

#include "common.h"
#include "state.h"
#include "vt100.h"
#include "uart.h"
#include "misc.h"
#include "bell.h"
#include "led.h"
#include "screen.h"

void vt100_init(void)
{
	vt100.margin.top = 0;
	vt100.margin.left = 0;
	vt100.margin.bottom = SCREEN_ROW - 1;
	vt100.margin.right = SCREEN_COL - 1;
	vt100.cursor.col = 0;
	vt100.cursor.row = 0;

	vt100.LOCAL = false;
	vt100_refresh_connect_mode();
}

void vt100_refresh_connect_mode(void)
{
	uart_loopback(vt100.LOCAL);

	if (vt100.LOCAL) {
		led_off(VT100_ONLINE);
		led_on(VT100_LOCAL);
	} else {
		led_off(VT100_LOCAL);
		led_on(VT100_ONLINE);
	}
}

/**
 * DECTST -- Invoke Confidence Test
 *
 * ESC [ 2 ; Ps y
 *
 * Ps is the parameter indicating the test to be done. Ps is computed by taking
 * the weight indicated for each desired test and adding them together. If Ps is
 * 0, no test is performed but the VT100 is reset.
 *
 * Test                                                  Weight
 *
 * Power up self-test (ROM check sum, RAM, NVR keyboard
 * and AVO if installed)                                   1
 *
 * Data Loop Back                                          2
 *                                            (loop back connector required)
 *
 * EIA modem control test                                  4
 *                                            (loop back connector required)
 * Repeat Selected Test(s)
 * indefinitely (until failure or power off)               8
 */
void vt100_DECTST(void)
{
	if (vt100.param.count >= 2) {
		return;
	}

	if (vt100.param.data[0] != 2) {
		return;
	}

	if (vt100.param.data[1] == 0) {
		vt100_reset();
		return; /* never reached */
	}

	repeat_test:

	/* TODO: test the terminal */

	if (vt100.param.data[1] & 8) {
		goto repeat_test;
	}
}

/**
 * RIS -- Reset To Initial State
 *
 * ESC c
 *
 * Reset the VT100 to its initial state, i.e., the state it has after it is powered on.
 * This also causes the execution of the power-up self-test and signal INIT H to be asserted briefly.
 */
void vt100_RIS(void)
{
	vt100_reset();
}

/**
 * Modes
 * The following is a list of VT100 modes which may be changed with set
 *  mode (SM) and reset mode (RM) controls.
 *
 * ANSI Specified Modes
 * Parameter    Mode Mnemonic    Mode Function
 *    0                          Error (ignored)
 *    20            LNM          Line feed new line mode
 */
static void ansi_mode_value(bool value)
{
	for (unsigned i = 0; i < vt100.param.count; i++) {
		switch (vt100.param.data[i]) {
		case 20:
			vt100.LNM = value;
		break;
		}
	}
}

/**
 * SM -- Set Mode
 *
 * ESC [ Ps ; . . . ; Ps h       default value: none
 *
 * Causes one or more modes to be set within the VT100 as specified by
 * each selective parameter in the parameter string. Each mode to be
 * set is specified by a separate parameter. A mode is considered set
 * until it is reset by a reset mode (RM) control sequence.
 */
void ansi_SM(void)
{
	ansi_mode_value(true);
}

/**
 * RM -- Reset Mode
 *
 * ESC [ Ps ; Ps ; . . . ; Ps l     default value: none
 *
 * Resets one or more VT100 modes as specified by each selective
 * parameter in the parameter string. Each mode to be reset is
 * specified by a separate parameter.
 */
void ansi_RM(void)
{
	ansi_mode_value(false);
}

/**
 * DEC Private Modes
 *
 * If the first character in the parameter string is ? (778), the
 * parameters are interpreted as DEC private parameters according to
 * the following:
 *
 * Parameter      Mode Mnemonic      Mode Function
 * 0                                 Error (ignored)
 * 1              DECCKM             Cursor key
 * 2              DECANM             ANSI/VT52
 * 3              DECCOLM            Column
 * 4              DECSCLM            Scrolling
 * 5              DECSCNM            Screen
 * 6              DECOM              Origin
 * 7              DECAWM             Auto wrap
 * 8              DECARM             Auto repeating
 * 9              DECINLM            Interlace
 */

static void vt100_mode_value(bool value)
{
	for (unsigned i = 0; i < vt100.param.count; i++) {
		switch (vt100.param.data[i]) {
		case 1:
			vt100.DECCKM = value;
		break;
		case 2:
			vt100.DECANM = value;
		break;
		case 3:
			vt100.DECCOLM = value;
		break;
		case 4:
			vt100.DECSCLM = value;
		break;
		case 5:
			vt100.DECSCNM = value;
			screen_invert(vt100.DECSCNM);
		break;
		case 6:
			vt100.DECOM = value;

			/* The cursor is moved to the new home position
			 * when this mode is set or reset. */
			if (vt100.DECOM) {
				/* Relative to top and left margin (Origin mode) */
				vt100.cursor.col = vt100.margin.left;
				vt100.cursor.row = vt100.margin.top;
			} else {
				/* Relative to top left corner */
				vt100.cursor.col = 0;
				vt100.cursor.row = 0;
			}

			bell_margin();
		break;
		case 7:
			vt100.DECAWM = value;
		break;
		case 8:
			vt100.DECARM = value;
		break;
		case 9:
			vt100.DECINLM = value;
		break;
		}
	}
}

/**
 * SM -- Set Mode
 *
 * ESC [ ? Ps ; . . . ; Ps h     default value: none
 *
 * Causes one or more modes to be set within the VT100 as specified by
 * each selective parameter in the parameter string. Each mode to be
 * set is specified by a separate parameter. A mode is considered set
 * until it is reset by a reset mode (RM) control sequence.
 */
void vt100_SM(void)
{
	vt100_mode_value(true);
}

/**
 * RM -- Reset Mode
 *
 * ESC [ ? Ps ; Ps ; . . . ; Ps l   default value: none
 *
 * Resets one or more VT100 modes as specified by each selective
 * parameter in the parameter string. Each mode to be reset is
 * specified by a separate parameter.
 */
void vt100_RM(void)
{
	vt100_mode_value(false);
}

/**
 * DECLL -- Load LEDS (DEC Private)
 *
 * ESC [ Ps q       default value: 0
 *
 * Load the four programmable LEDs on the keyboard according to the parameter(s).
 *
 * Parameter    Parameter Meaning
 *   0          Clear LEDs L1 through L4
 *   1          Light L1
 *   2          Light L2
 *   3          Light L3
 *   4          Light L4
 *
 * LED numbers are indicated on the keyboard.
 */
void vt100_DECLL(void)
{
	if (!vt100.param.count) {
		vt100.param.count = 1;
		vt100.param.data[0] = 0;
	}

	for (unsigned i = 0; i < vt100.param.count; i++) {
		switch (vt100.param.data[i]) {
		case 0:
			led_off(VT100_L1 | VT100_L2 | VT100_L3 | VT100_L4);
		break;
		case 1:
			led_on(VT100_L1);
		break;
		case 2:
			led_on(VT100_L2);
		break;
		case 3:
			led_on(VT100_L3);
		break;
		case 4:
			led_on(VT100_L4);
		break;
		}
	}
}

/**
 * XON/XOFF is a software flow control mechanism.
 *
 * XOFF is used to tell receiver that it should not send more data.
 * XON is used to tell receiver that the sender can accept more data.
 *
 * When XON is send to a terminal, it can send data.
 * When XOFF is send to terminal, it should not send data.
 *
 * In our case, XON = 0x11 and XOFF = 0x13
 */

/**
 * Causes terminal to resume transmission.
 */
void vt100_XON(void)
{
	vt100.XOFFED = false;
}

/**
 * Causes terminal to stop transmitted all codes except XOFF and XON.
 */
void vt100_XOFF(void)
{
	if (!vt100.LOCAL) {
		vt100.XOFFED = true;
	}
}

/**
 * SCS – Select Character Set
 *
 * The appropriate G0 and G1 character sets are designated from one of
 * the five possible character sets. The G0 and G1 sets are invoked by
 * the codes SI and SO (shift in and shift out) respectively.
 *
 * G0 Sets Sequence   G1 Sets Sequence   Meaning
 * ESC ( A            ESC ) A            United Kingdom Set
 * ESC ( B            ESC ) B            ASCII Set
 * ESC ( 0            ESC ) 0            Special Graphics
 * ESC ( 1            ESC ) 1            Alternate Character ROM
 *                                            Standard Character Set
 * ESC ( 2            ESC ) 2            Alternate Character ROM
 *                                            Special Graphics
 */

static inline void update_data_prop_char_set(void)
{
	vt100.data_prop.char_set = vt100.CHAR_SET_SO ? vt100.G1 : vt100.G0;
}

void vt100_char_set(bool G0, enum _vt100_char_set char_set)
{
	if (G0) {
		vt100.G0 = char_set;
	} else {
		vt100.G1 = char_set;
	}

	update_data_prop_char_set();
}

void vt100_shift_out(bool SO)
{
	vt100.CHAR_SET_SO = SO;
	update_data_prop_char_set();
}
