/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef VT100_MISC_H
#define VT100_MISC_H

#include "vt100.h"

void vt100_refresh_connect_mode(void);

void vt100_RIS(void);
void vt100_init(void);

void vt100_RM(void);
void vt100_SM(void);

void ansi_RM(void);
void ansi_SM(void);

void vt100_DECTST(void);
void vt100_DECLL(void);

void vt100_XOFF(void);
void vt100_XON(void);

void vt100_char_set(bool G0, enum _vt100_char_set char_set);
void vt100_shift_out(bool SO);

#endif
