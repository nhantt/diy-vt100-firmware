/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt100/report.h"
#include "uart.h"
#include "state.h"
#include "vt100.h"
#include "vt100/tab.h"
#include "vt100/misc.h"
#include "vt100.h"
#include "vt100/cursor.h"
#include "../misc.h"
#include "debug.h"
#include "config.h"

/**
 * DECREPTPARM -- Report Terminal Parameters
 * ESC [ <sol>; <par>; <nbits>; <xspeed>; <rspeed>; <clkmul>; <flags> x
 *
 * These sequence parameters are explained below in the DECREQTPARM sequence.
 * DECREQTPARM -- Request Terminal Parameters
 * ESC [ <sol> x
 *
 * The sequence DECREPTPARM is sent by the terminal controller to notify the host
 * of the status of selected terminal parameters. The status sequence may be
 * sent when requested by the host or at the terminal's discretion. DECREPTPARM
 * is sent upon receipt of a DECREQTPARM. On power-up or reset, the VT100 is
 * inhibited from sending unsolicited reports.
 *
 * The meanings of the sequence parameters are:
 *
 * Parameter      Value      Meaning
 *
 *   <sol>      0 or none    This message is a request (DECREQTPARM) and the
 *                           terminal will be allowed to send unsolicited
 *                           reports. (Unsolicited reports are sent when the
 *                           terminal exits the SET-UP mode).
 *
 *                  1        This message is a request; from now on the terminal
 *                           may only report in response to a request.
 *
 *                  2        This message is a report (DECREPTPARM).
 *
 *                  3        This message is a report and the terminal is only
 *                           reporting on request.
 *
 *   <par>          1        No parity set
 *                  4        Parity is set and odd
 *                  5        Parity is set and even
 *
 *  <nbits>         1        8 bits per character
 *                  2        7 bits per character
 *
 * <xspeed>, <rspeed> 0      50 Bits per second
 *                    8      75 Bits per second
 *                    16     110 Bits per second
 *                    24     134.5 Bits per second
 *                    32     150 Bits per second
 *                    40     200 Bits per second
 *                    48     300 Bits per second
 *                    56     600 Bits per second
 *                    64     1200 Bits per second
 *                    72     1800 Bits per second
 *                    80     2000 Bits per second
 *                    88     2400 Bits per second
 *                    96     3600 Bits per second
 *                    104    4800 Bits per second
 *                    112    9600 Bits per second
 *                    120    19200 Bits per second
 *
 *  <clkmul>        1        The bit rate multiplier is 16.
 *
 *  <flags>        0-15      This value communicates the four switch values in
 *                           block 5 of SET UP B, which are only visible to the
 *                           user when an STP option is installed. These bits
 *                           may be assigned for an STP device. The four bits
 *                           are a decimal-encoded binary number.
 */
void vt100_DECREPTPARAM(void)
{
	/* TODO: send unsolicted message to host after exiting from SET-UP */

	vt100.UNSOLIC = !(vt100.param.count && vt100.param.data[0]);

	uart_send(ASCII_ESCAPE);
	uart_send('[');

	uart_send(vt100.UNSOLIC ? '2' : '3');

	uart_send(';');
	/* parity info's */
	if (vt100.PARITY) {
		uart_send(vt100.PARITY_SENSE ? '5' : '4');
	} else {
		/* is this required?? */
		uart_send('1');
	}

	uart_send(';');
	uart_send(vt100.BPC ? '1' : '2');

	uart_send(';');
	uart_send_string(vt100_uart_tx_speed[vt100.uart_tx].value);

	uart_send(';');
	uart_send_string(vt100_uart_rx_speed[vt100.uart_rx].value);

	uart_send(';');
	uart_send(vt100_uart_tx_speed[vt100.uart_tx].clkmul);

	uart_send(';');
	uart_send('0');

	uart_send('x');
}

/**
 * DSR -- Device Status Report
 *
 * ESC [ Ps n          default value: 0
 *
 * Requests and reports the general status of the VT100 according to the
 * following parameter(s).
 *
 * Parameter    Parameter Meaning
 *
 *    0         Response from VT100 -- Ready, No malfunctions detected (default)
 *
 *    3         Response from VT100 -- Malfunction -- retry
 *
 *    5         Command from host -- Please report status
 *              (using a DSR control sequence)
 *
 *    6         Command from host -- Please report active position
 *              (using a CPR control sequence)
 *
 * DSR with a parameter value of 0 or 3 is always sent as a response to a
 * requesting DSR with a parameter value of 5.
 */
void vt100_DSR(void)
{
	uart_send(ASCII_ESCAPE);
	uart_send('[');

	switch (vt100.param.data[0]) {
	case 5:
		/* report status
		 * sending using DSR control sequence */
		uart_send(vt100_malfunctioning() ? '3' : '0');
		uart_send('n');
	break;
	case 6: {
		/* report active position
		 * sending using CPR control sequence */
		vt100_row row;
		vt100_col col;

		if (vt100.DECOM) {
			/* Calculate value with respect to left and top margin */
			/* FIXME: how to handle underflow? */
			row = vt100.cursor.row - vt100.margin.top;
			col = vt100.cursor.col - vt100.margin.left;
		} else {
			/* Values according to top left cornet */
			row = vt100.cursor.row;
			col = vt100.cursor.col;
		}

		uart_send_uint8(row + 1);
		uart_send(';');
		uart_send_uint8(col + 1);
		uart_send('R');
	} break;
	}
}

/**
 * DA -- Device Attributes
 *
 * ESC [ Pn c         default value: 0
 *
 * The host requests the VT100 to send a device attributes (DA)
 * control sequence to identify itself by sending the DA
 * control sequence with either no parameter or a parameter of 0.
 *
 * Response to the request described above (VT100 to host) is generated
 * by the VT100 as a DA control sequence with the numeric parameters as
 * follows:
 *
 *   Option Present                 Sequence Sent
 *   No options                     ESC [?1;0c
 *   Processor option (STP)         ESC [?1;1c
 *   Advanced video option (AVO)    ESC [?1;2c
 *   AVO and STP                    ESC [?1;3c
 *   Graphics option (GPO)          ESC [?1;4c
 *   GPO and STP                    ESC [?1;5c
 *   GPO and AVO                    ESC [?1;6c
 *   GPO, STP and AVO               ESC [?1;7c
 *
 * No options (Vanilla)
 *  - 80x24 or 132x14
 *  - Single type of character attribute is possible for each character
 *      Single bit used for Underscore show/not-show if Cursor selection = Underline style
 *      Single bit used for Inverse show/not-show if Cursor selection = Block style
 *  - Single type of character set is possible for all data
 *
 * STP -- Standard Terminal Port
 * This is an edge connector on the main board that contains breakout
 * points for the RS232 and Modem lines. It can be used to disconnect
 * the terminal from the normal serial port and connect it to an
 * internal device. The external serial port is also available to the
 * device as a plain connector.
 * Source: https://github.com/phooky/VT100-Hax/blob/master/Platform%20Notes.md
 *
 * AVO -- Advance Video Option
 *  - 80x24 or 132x24
 *  - Multiple types of character attribute is possible for each data.
 *      - blink
 *      - bold
 *      - underline
 *      - inverse
 *  - Multiple type of character attribute is possible for each character
 *
 * GPO -- Graphics Processor Option
 *  Addd "Waveform graphics" capability
 *  https://en.wikipedia.org/wiki/Waveform_graphics
 */
void vt100_DECID(void)
{
	if (vt100.param.count && vt100.param.data[0]) {
		LOGF("ESC [ Pn c    WHERE Pn should be 0 but instead host has "
			"send Pn=%"PRIu8, vt100.param.data[0]);
	}

	uart_send(ASCII_ESCAPE);
	uart_send('[');
	uart_send('?');
	uart_send('1');
	uart_send(';');
#if (AVO_INSTALLED == 1)
	uart_send('2'); /* AVO */
#else
	uart_send('0'); /* No Options */
#endif
	uart_send('c');
}
