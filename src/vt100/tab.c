/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <string.h>
#include "vt100/tab.h"
#include "vt100/buffer.h"
#include "vt100/tab.h"
#include "vt100/cursor.h"
#include "state.h"
#include "setup.h"
#include "vt100.h"
#include "bell.h"
#include "generic/bit-array.h"

/**
 * TBC -- Tabulation Clear  [Format Effector]
 *
 * ESC [ Ps g          default value: 0
 *
 * Parameter    Parameter Meaning
 *    0         Clear the horizontal tab stop at the active position
 *              (the default case).
 *
 *    3         Clear all horizontal tab stops.
 *
 * Any other parameter values are ignored.
 */
void vt100_TBC(void)
{
	uint8_t v = vt100.param.count ? vt100.param.data[0] : 0;

	switch (v) {
	case 0:
		/* clear the horizontal tab stop at the current position */
		bitarr_low(vt100.tab_stop, vt100.cursor.col);
	break;
	case 3:
		/* clear all horizontal tab stops */
		memset(vt100.tab_stop, 0, sizeof(vt100.tab_stop));
	break;
	}
}

/**
 * HTS -- Horizontal Tabulation Set  [Format Effector]
 *
 * ESC H
 *
 * Set one horizontal stop at the active position.
 */
void vt100_HTS(void)
{
	bitarr_high(vt100.tab_stop, vt100.cursor.col);
}

/**
 * HT -- Horizontal Tabulation
 *
 * Move the cursor to the next tab stop, or to the right margin
 *  if no further tab stops are present on the line.
 */
void vt100_HT(void)
{
	for (vt100_col j = vt100.cursor.col + 1; j < vt100.margin.right; j++) {
		if (bitarr_read(vt100.tab_stop, j)) {
			/* Found a tab stop */
			vt100.cursor.col = j;
			bell_margin();
			return;
		}
	}

	/* Move to right margin */
	vt100.cursor.col = vt100.margin.right;
}
