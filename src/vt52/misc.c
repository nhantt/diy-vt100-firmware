/*
 * This file is part of diy-VT100.
 * Copyright (C) 2016 Kuldeep Singh Dhaka <kuldeep@madresistor.com>
 *
 * diy-VT100 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * diy-VT100 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with diy-VT100.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "vt52/misc.h"
#include "uart.h"
#include "vt100.h"

/**
 * DECID -- Identify Terminal (DEC Private)
 *
 * ESC Z
 *
 * This sequence causes the same response as the ANSI device attributes (DA).
 * This sequence will not be supported in future DEC terminals, therefore, DA
 * should be used by any new software.
 */
void vt52_ident(void)
{
	uart_send(ASCII_ESCAPE);
	uart_send('/');
	uart_send('Z');
}

/**
 * Enter ANSI Mode
 *
 * ESC <
 *
 * All subsequent escape sequences will be interpreted according to ANSI
 * Standards X3.64-1977 and X3.41-1974. The VT52 escape sequence designed in
 * this section will not be recognized.
*/
void vt52_enter_ansi_mode(void)
{
	vt100.DECANM = true;
}

/**
 * DECKPAM -- Keypad Application Mode (DEC Private)
 *
 * ESC =
 *
 * The auxiliary keypad keys will transmit control sequences.
 */
void vt52_DECKPAM(void)
{
	/* Note: USB and PS/2 keyboard do not have a ',' [comma] on keypad
	 * It is not possible to send comma from keypad [Keypad Application Mode].
	 * User can only use the other comma key (near space bar) to fake [Keypad Numeric Mode].
	 */
	vt100.DECKPAM = true;
}

/**
 * DECKPNM -- Keypad Numeric Mode (DEC Private)
 *
 * ESC >
 *
 * The auxiliary keypad keys will send ASCII codes corresponding
 *  to the characters engraved on the keys.
 */
void vt52_DECKPNM(void)
{
	vt100.DECKPAM = false;
}

static void vt52_char_set(enum _vt100_char_set char_set)
{
#if (AVO_INSTALLED == 1)
	vt100.data_prop.char_set = char_set;
#else
	vt100.char_set = char_set;
#endif
}

/**
 * Enter Graphics Mode
 *
 * ESC F
 *
 * Causes the special graphics character set to be used.
 */
void vt52_enter_graphics_mode(void)
{
	vt52_char_set(VT100_CHAR_SPECIAL_GRAPHICS);
}

/**
 * Exit Graphics Mode
 *
 * ESC G
 *
 * This sequence causes the standard ASCII character set to be used.
 */
void vt52_exit_graphics_mode(void)
{
	vt52_char_set(VT100_CHAR_ASCII);
}
